public class Sep26 {

    public static void main(String[] args) {

        int[] myList = new int[]{-1, 2, 3, 4};
        double[][] myArray = {{12.0, 7.5, -34.3}, {1.0, 19.8, -2.6}, {5.2, 10.8, 3.0}};

        System.out.println(task01(myList));
        System.out.println(task02(myList));
        System.out.println(task03(myList));
        System.out.println(task04(myList));
        System.out.println(task05(myList));
        System.out.println(task06(myList));
        System.out.println(task07(myList));
        System.out.println(task08(myList));
        System.out.println(task09(myList));
        System.out.println(task10(myList));
        System.out.println(task11(myList));
        System.out.println(task12(myList));
        System.out.println(task13(myList));
        System.out.println(task14(myList));
        System.out.println(task15(myList));
        System.out.println(task16(myList));
        task17(myList);
        System.out.println(task18(myArray));
        System.out.println(task19(myArray));
        System.out.println(task20(myArray));
        System.out.println(task21(myArray));
        System.out.println(task22(myArray));
        System.out.println(task23(myArray));
        System.out.println(task24(myArray));
        task26(myArray);
        task27(myArray);
    }

    // Task 01
    public static String task01(int[] myList) {
        int min = myList[0];
        for (
                int i = 1;
                i < myList.length; i++) {
            if (myList[i] < min) min = myList[i];
        }
        return "Задание 01. Наименьший элемент: " + min;
    }

    // Task 02
    public static String task02(int[] myList) {
        int max = myList[0];
        for (int i = 1; i < myList.length; i++) {
            if (myList[i] > max) max = myList[i];
        }
        return "Задание 02. Наибольший элемент: " + max;
    }

    // Task 03
    public static String task03(int[] myList) {
        int total = 0;
        for (int element : myList) {
            total += element;
        }
        return "Задание 03. Сумма чисел массива: " + total;
    }

    // Task04
    public static String task04(int[] myList) {
        int total = 0;
        for (double element : myList) {
            total += element;
        }
        double middle = (double) total / myList.length;
        return "Задание 04. Среднее арифметическое: " + middle;
    }

    // Task 05
    public static String task05(int[] myList) {
        int multipple = 1;
        for (int element : myList) {
            multipple *= element;
        }
        return "Задание 05. Произведение элементов: " + multipple;
    }

    // Task 06
    public static String task06(int[] myList) {
        int odd = 0;
        for (int i = 1; i < myList.length; i += 2)
            odd += myList[i];
        return "Задание 06. Сумма нечетных чисел: " + odd;
    }

    // Task 07
    public static String task07(int[] myList) {
        int even = 0;
        for (int i = 0; i < myList.length; i += 2) {
            even += myList[i];
        }
        return "Задание 07. Сумма четных чисел: " + even;
    }

    // Task 08
    public static String task08(int[] myList) {
        int multippleOdd = 1;
        for (int i = 1; i < myList.length; i += 2) {
            multippleOdd *= myList[i];
        }
        return "Задание 08. Произведение нечетных чисел: " + multippleOdd;
    }

    // Task 09
    public static String task09(int[] myList) {
        int multippleEven = 1;
        for (int i = 0; i < myList.length; i += 2) {
            multippleEven *= myList[i];
        }
        return "Задание 09. Произведение четных чисел: " + multippleEven;
    }

    // Task 10
    public static String task10(int[] myList) {
        int zero = 0;
        for (int i = 0; i < myList.length; i++) {
            if (myList[i] == 0)
                zero++;
        }
        return "Задание 10. Кол-во нулей: " + zero;
    }

    // Task 11
    public static String task11(int[] myList) {
        int odd = 0;
        for (int i = 1; i < myList.length; i += 2)
            odd++;

        return "Задание 11. Кол-во нечетных элементов: " + odd;
    }

    // Task 12
    public static String task12(int[] myList) {
        int odd = 0;
        for (int i = 0; i < myList.length; i += 2)
            odd++;

        return "Задание 12. Кол-во четных элементов: " + odd;
    }

    // Task 13
    public static String task13(int[] myList) {
        int k = 2;
        int sum = 0;
        for (int i = 0; i < myList.length; i++)
            if (myList[i] > k)
                sum++;
        return "Задание 13. Кол-во элементов, превышающих " + k + ": " + sum;
    }

    // Task 14
    public static String task14(int[] myList) {
        int k = 2;
        int sum = 0;
        for (int i = 0; i < myList.length; i++)
            if (myList[i] <= k)
                sum++;
        return "Задание 14. Кол-во элементов, не превышающих " + k + ": " + sum;
    }

    // Task 15
    public static String task15(int[] myList) {
        int k = 2;
        int sum = 0;
        for (int i = 1; i < myList.length; i++)
            if (myList[i] <= k)
                sum++;
        return "Задание 15. Кол-во элементов, превышающих " + k + ": " + sum;
    }

    // Task 16
    public static String task16(int[] myList) {
        int x = myList[0];
        for (int i = 1; i < myList.length; i++)
            if (x >= Math.abs(myList[i]) - 0)
                x = myList[i];

        return "Задание 16. Ближайший к нулю элемент: " + x;
    }

    // Task 17
    static void task17(int[] myList) {
        System.out.print("Задание 17. Нормированные элементы: ");
        double max = myList[0];
        for (int i = 0; i < myList.length; i++)
            max = Math.max(max, myList[i]);

        for (int element : myList)
            System.out.print((double) (element / max) + "   ");
    }

    // Task 18
    public static String task18(double[][] myArray) {
        double min = myArray[0][0];
        for (int i = 0; i < myArray.length; i++)
            for (int j = 0; j < myArray[i].length; j++) {
                if (myArray[i][j] < min)
                    min = Math.min(min, myArray[i][j]);
            }
        return "\nЗадание 18. Минимальное значение: " + min;
    }

    // Task 19
    public static String task19(double[][] myArray) {
        double max = myArray[0][0];
        for (int i = 0; i < myArray.length; i++)
            for (int j = 0; j < myArray[i].length; j++) {
                if (myArray[i][j] > max)
                    max = Math.max(max, myArray[i][j]);
            }
        return "Задание 19. Максимальное значение: " + max;
    }

    // Task 20
    public static String task20(double[][] myArray) {
        double sum = 0.0;
        for (int i = 0; i < myArray.length; i++)
            for (int j = 0; j < myArray[i].length; j++) {
                sum += myArray[i][j];
            }
        return "Задание 20. Сумма: " + sum;
    }

    // Task 21
    public static String task21(double[][] myArray) {
        double sum = 0.0;

        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                sum += myArray[i][j];
            }
        }
        double middle = sum / (myArray.length * myArray[0].length);
        return "Задание 21. Среднее арифметическое: " + middle;
    }

    // Task 22
    public static String task22(double[][] myArray) {
        double sum = 0.0;
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                if (i == j)
                    sum += myArray[i][j];
            }
        }
        return "Задание 22. Сумма по диагонали: " + sum;
    }

    // Task 23
    public static String task23(double[][] myArray) {
        double sum = 0.0;
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                if (j == myArray[i].length - 1 - i)
                    sum += myArray[i][j];
            }
        }
        return "Задание 23. Сумма по побочной диагонали: " + sum;
    }

    // Task 24
    public static String task24(double[][] myArray) {
        double determinant = 0.0;
        double a;
        double b;
        for (int j = 0; j < myArray.length; j++) {
            a = 1.0;
            b = 1.0;
            for (int i = 0; i < myArray[j].length; i++) {
                a *= myArray[i][(j + i) % myArray.length];
                b *= myArray[myArray.length - i - 1][(j + i) % myArray.length];
            }
            determinant += a - b;

        }
        return "Задание 24. Определитель квадратного двумерного массива: " + determinant;
    }

    // Task 26
    static void task26(double[][] myArray) {
        System.out.println("Задание 26. Нормированные элементы в двумерном массиве: ");
        double min = myArray[0][0];
        double max = myArray[0][0];
        for (int i = 0; i < myArray.length; i++)
            for (int j = 0; j < myArray[i].length; j++) {
                if (myArray[i][j] > min)
                    max = Math.max(max, myArray[i][j]);
            }
        double[][] cloneArr = new double[myArray.length][myArray[0].length];
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                cloneArr[i][j] = myArray[i][j] / max;
                System.out.print(cloneArr[i][j] + (j == myArray.length - 1 ? "\n" : "     "));
            }
        }
    }

    // Task 27
    static void task27(double[][] myArray) {
        System.out.println("Задание 27. Транспонированный квадратный массив: ");
        double trans;
        for (int i = 0; i < myArray.length; i++)
            for (int j = i + 1; j < myArray[i].length; j++) {
                trans = myArray[i][j];
                myArray[i][j] = myArray[j][i];
                myArray[j][i]= trans;
            }
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                System.out.print(myArray[i][j] + (j == myArray.length - 1 ? "\n" : "     "));
            }
        }
    }

}
