public class Oct1 {
    public static void main(String[] args) {
        //Task 01
        //Создать символьный массив, содержащий латинский алфавит
        char[] arr = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        System.out.println("Задание 01: ");
        for (char element : arr) {
            System.out.print(element + "\t");
        }
        System.out.println();

        System.out.println("Наш массив:");
        int[] myInt = new int[10];
        for (int i = 0; i < myInt.length; i++) {
            myInt[i] = ((int) (Math.random() * 12) + 1);
            System.out.print(myInt[i] + "\t");
        }
        System.out.println();

        System.out.println(task2(myInt));
        System.out.println(task3(myInt));
        System.out.println(task4(myInt));

        System.out.println("Наш двумерный массив:");
        double[][] myArray = new double[4][4];
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                myArray[i][j] = ((double) (Math.random() * 12.0) + 1.0);
                System.out.print(myArray[i][j] + (j == myArray.length - 1 ? "\n" : "     "));
            }
        }

        System.out.println(task5(myArray));

        //Task 06
        //Создать и заполнить единицами произвольный трехмерный массив (решить
        //задачу с помощью while)
        System.out.println("Наш трёхмерный массив:");
        int[][][] myArr = new int[3][3][3];
        int i = 0;
        int j = 0;
        int k = 0;
        while (i < myArr.length) {
            j = 0;
            while (j < myArr[i].length) {
                k = 0;
                while (k < myArr[i][j].length) {
                    myArr[i][j][k] = 1;
                    System.out.print(myArr[i][j][k] + "\t");
                    k++;
                }
                System.out.println();
                j++;
            }
            System.out.println();
            i++;
        }

        System.out.println(task7(myArr));
        System.out.println(task8(myArr));
        System.out.println("Задание 9. (проверить, что все элементы в массиве расположены по возрастанию):");
        task9(myInt);
        System.out.println("Задание 10. (найти три самых больших элемента в массиве):");
        task10(myInt);
        System.out.println("Задание 11. (перевернуть массив):");
        task11(myInt);
        System.out.println("Задание 12. (Осуществить циклический сдвиг элементов массива вправо):");
        task12(myInt);
        System.out.println("Задание 13. (найти минимальный элемент):" + task13(myInt));
        System.out.println("Задание 14. (найти максимальный элемент):" + task14(myInt));
        System.out.println("Задание 15. (проверить, все ли элементы в массиве различны):" + task15(myInt));
        System.out.println(task16(myInt));
        System.out.println("Задание 17. (найти количество элементов, равных нулю):" + task17(myInt));
        System.out.println(task18(myInt));
        System.out.println("Задание 19. (Нормировать все элементы относительно суммы положительных эл-в):");
        task19(myInt);
        System.out.println("Задание 20. (найти сумму элементов двумерного массива):" + task20(myArray));
        System.out.println("Задание 21. (найти среднее арифметическое двумерного массива):" + task21(myArray));
        System.out.println(task22(myArray));
        System.out.println("Задание 23. (Нормировать все элементы относительно максимального по модулю элемента):");
        task23(myArray);
        System.out.println("Задание 24. (Поделить на два все элементы двумерного массива, большие 10 по модулю):");
        task24(myArray);
        System.out.println("Трехмерная матрица:");
        System.out.println(task25(myArr));
        System.out.println("Задание 26. Поменять знак у всех отрицательных элементов");
        task26(myArr);
        System.out.println("Задание 27.\n");
        System.out.println(task27(myArr));
        System.out.println("Задание 28.\n");
        task28(myArr);
        System.out.println("Задание 29. Сортировка xy по сумме элементов\n");
        task29(myArr);
        System.out.println("Задание 30. Элементы трехмерного массива в строку\n");
        task30(myArr);
        System.out.println("\n\nЗадание 31. Сортировка xy по максимальному элементу\n");
        task31(myArr);
        System.out.println("\nЗадание 32. xy-плоскости с z-координатой\n");
        task32(myArr);
        System.out.println("\nЗадание 33. Средние арифметические xy-плоскостей\n");
        task33(myArr);
        System.out.println("\nЗадание 34. Отсортировать по возрастанию xy-плоскости\n");
        task34(myArr);

    }

    //Task 02
    //Найти максимальное значение в целочисленном массиве из n-элементов
    //(решить задачу с помощью for)
    static String task2(int[] myInt) {
        int max = myInt[0];
        for (int i = 1; i < myInt.length; i++) {
            if (myInt[i] > max) max = myInt[i];
        }
        return "Задание 02. Максимальное значение в целочисленном массиве: " + max;
    }

    //Task 03
    //Найти минимальное значение в целочисленном массиве из n-элементов (решить
    //задачу с помощью for)
    static String task3(int[] myInt) {
        int min = myInt[0];
        for (int i = 1; i < myInt.length; i++) {
            if (myInt[i] < min) min = myInt[i];
        }
        return "Задание 03. Минимальное значение в целочисленном массиве: " + min;
    }

    //Task 04
    /*В целочисленном массиве из n-элементов найти сумму элементов, следующих
    после элемента, равного единице, вывести эту сумму; если такой элемент
    отсутствует, вывести ноль*/
    static String task4(int[] myInt) {
        int sum = 0;
        for (int i = 1; i < myInt.length - 1; i++) {
            if (myInt[i] == 1) {
                sum += i + 1;
            } else
                sum = 0;
        }
        return "Задание 04.  Сумма элементов, следующих после \'1\': " + sum;
    }

    //Task 05
    /*В произвольной квадратной матрице найти минимальный элемент на главной
      диагонали*/
    static String task5(double[][] myArray) {
        double sum = 0.0;
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                if (i == j)
                    sum += myArray[i][j];
            }
        }
        return "Задание 05. Сумма по диагонали: " + sum;
    }

    //Task 07
    /*Проверить, что все элементы в массиве одинаковы. Вывести “true” если
    одинаковы и “false” если нет.*/
    static String task7(int[][][] myArr) {
        boolean equal;
        int b = 0;
        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length - 1; k++) {
                    if (myArr[i][j][k] == myArr[i][j][k + 1]) {
                    } else
                        b++;
                }
            }
        }
        if (b != 0)
            equal = false;
        else
            equal = true;

        return "Задание 07. Все элементы одинаковы: " + equal;
    }

    //Task 08
    //Найти сумму положительных элементов массива.
    static String task8(int[][][] myArr) {
        int sum = 0;
        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    if (myArr[i][j][k] >= 0)
                        sum += myArr[i][j][k];
                }
            }
        }
        return "Задание 08. Сумма положительных элементов : " + sum;
    }

    //Task 09
    //Проверить, что все элементы в массиве расположены по возрастанию.
    static void task9(int[] myInt) {
        int temp;
        for (int i = 0; i < myInt.length - 1; i++) {
            for (int j = 0; j < myInt.length - 1 - i; j++) {
                if (myInt[j] > myInt[j + 1]) {
                    temp = myInt[j];
                    myInt[j] = myInt[j + 1];
                    myInt[j + 1] = temp;
                }
            }
        }
        for (int i = 0; i < myInt.length; i++) {
            System.out.print(myInt[i] + "  ");
        }
        System.out.println("");

    }

    //Task 10
    /*Найти три самых больших элемента в массиве. Например, для массива
    [5,1,4,8,3,5,2,7] ответ будет 8, 7, 5.*/
    static void task10(int[] myInt) {
        task9(myInt);
        for (int i = myInt.length - 3; i < myInt.length; i++) {
            System.out.print(myInt[i] + "  ");
        }
        System.out.println("");
    }

    //Task 11
    //Перевернуть массив. Например, [2, 3, 5] превращается в [5, 3, 2].
    static void task11(int[] myInt) {
        int tmp;
        for (int i = 0; i < myInt.length - 1; i++) {
            for (int j = 0; j < myInt.length - 1 - i; j++) {
                tmp = myInt[j];
                myInt[j] = myInt[j + 1];
                myInt[j + 1] = tmp;
            }
        }

        for (int i = 0; i < myInt.length; i++) {
            System.out.print(myInt[i] + "  ");
        }
        System.out.println("");
    }

    //Task 12
    /*Осуществить циклический сдвиг элементов массива вправо. Например, [1, 5, 3,
            7] после сдвига будет выглядеть так: [7, 1, 5, 3].*/
    static void task12(int[] myInt) {
        int tmp;
        for (int i = 0; i < myInt.length - 1; i++) {
            //for (int j = 0; j < myInt.length - 1 - i; j++) {
            tmp = myInt[i];
            myInt[i] = myInt[i + 1];
            myInt[i + 1] = tmp;
            // }
        }

        for (int i = 0; i < myInt.length; i++) {
            System.out.print(myInt[i] + "  ");
        }
        System.out.println("");

    }

    //Task 13
    //Найти минимальный элемент в одномерном массиве.
    static int task13(int[] myInt) {
        int min = myInt[0];
        for (int i = 1; i < myInt.length; i++) {
            if (min > myInt[i]) {
                min = myInt[i];
            }
        }
        return min;
    }

    //Task 14
    //Найти максимальный элемент в одномерном массиве.
    static int task14(int[] myInt) {
        int max = myInt[0];
        for (int i = 1; i < myInt.length; i++) {
            if (max < myInt[i]) {
                max = myInt[i];
            }
        }
        return max;
    }

    //Task 15
    //Проверить, все ли элементы в массиве различны. Вывести “true” или “false”
    //соответственно.
    static boolean task15(int[] myInt) {
        boolean rez = false;
        for (int i = 0; i < myInt.length - 1; i++) {
            if (myInt[i] != myInt[i + 1]) {
                rez = true;
                break;
            }
        }
        return rez;
    }

    //Task 16
    //Определить количество различных элементов в массиве.
    static String task16(int[] myInt) {
        //int[] myArrayInt1 = {1, 6, 8, 5, 5, 5, 3, -1, 8, 8}; //!
        task9(myInt);
        int col = myInt.length;
        for (int i = 0; i < myInt.length - 1; i++) {
            if (myInt[i] == myInt[i + 1]) {
                col--;
            }
        }
        return "Задание 16. (Определить количество различных элементов в массиве):" + col;
    }

    //Task 17
    //Найти количество элементов, равных нулю, в одномерном массиве.
    static int task17(int[] myInt) {
        int col = 0;
        for (int i = 0; i < myInt.length; i++) {
            if (myInt[i] == 0) {
                col++;
            }
        }
        return col;
    }

    //Task 18
    //Найти количество элементов, меньших заданного числа k, в одномерном
    //массиве.
    static String task18(int[] myInt) {
        int col = 0;
        int k = -2;
        for (int i = 0; i < myInt.length; i++) {
            if (myInt[i] < k) {
                col++;
            }
        }
        return "Задание 18. (найти количество элементов, меньших заданного числа \"" + k + "\"):" + col;
    }

    //Task 19
    /*Нормировать все элементы в одномерном массиве из n-элементов, каждый из
    которых больше, либо равен нулю, относительно их суммы.*/
    static void task19(int[] myInt) {
        int sum = 0;
        for (int element : myInt) {
            if (element >= 0) {
                sum += element;
            }
        }
        for (int element : myInt) {
            if (element >= 0) {
                System.out.print((float) element / sum + "  ");
            }
        }
        System.out.println();
    }

    //Task 20
    //Найти сумму элементов двумерного массива из n-элементов.
    static double task20(double[][] myArray) {
        double sum = 0.0;
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                sum += myArray[i][j];
            }
        }
        return sum;
    }

    //Task 21
    //Найти среднее арифметическое двумерного массива из n-элементов.
    static float task21(double[][] myArray) {
        return (float) task20(myArray) / (float) (myArray.length * myArray[0].length);
    }

    //Task 22
    /*Найти сумму элементов на главной и побочной диагоналях в квадратном
    двумерном массиве.*/
    static String task22(double[][] myArray) {
        int sumG = 0;
        int sumP = 0;
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                if (i == j)
                    sumG += myArray[i][j];
                if (j == myArray[i].length - 1 - i)
                    sumP += myArray[i][j];
            }
        }

        return "Задание 22. Найти сумму на главной даигонали = " + sumG + " и побочной = " + sumP;
    }

    //Task 23
    /*Нормировать все элементы квадратного двумерного массива относительно
    максимального по модулю элемента.*/
    static void task23(double[][] myArray) {
        double max = myArray[0][0];
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                if (Math.abs(myArray[i][j]) > max) {
                    max = Math.abs(myArray[i][j]);
                }
            }
        }
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[0].length; j++) {
                System.out.print((float) myArray[i][j] / max + "  ");
            }
            System.out.println();
        }
    }

    //Task 24
    /*Поделить на два все элементы двумерного массива, большие десяти по
    модулю.*/
    static void task24(double[][] myArray) {
        double rez = 0.0;
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                if (Math.abs(myArray[i][j]) > 10) {
                    rez = (float) myArray[i][j] / 2;
                    System.out.print(rez + "  ");
                } else
                    System.out.print((float) myArray[i][j] + "  ");
            }
            System.out.println();
        }
    }

    //Task 25
    //Найти сумму элементов трехмерного массива.
    static String task25(int[][][] myArr) {
        int sum = 0;
        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    myArr[i][j][k] = ((int) (Math.random() * 12 - 5));
                    sum += myArr[i][j][k];
                }
            }
        }
        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    System.out.print(myArr[i][j][k] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }
        return "Задание 25. (найти сумму элементов трехмерного массива): " + sum;
    }

    //Task 26
    //Поменять знак у всех отрицательных элементов трехмерного массива.
    static void task26(int[][][] myArr) {
        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    if (myArr[i][j][k] < 0) {
                        myArr[i][j][k] = myArr[i][j][k] * -1;
                    }

                }
            }
        }
        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    System.out.print(myArr[i][j][k] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    //    Task 27
    /*Вычислить среднее арифметическое для всех неотрицательных элементов
    трехмерного массива.*/
    static String task27(int[][][] myArr) {
        int count = 0;
        int sum = 0;
        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    myArr[i][j][k] = (int) (Math.random() * 12 - 5);
                    if (myArr[i][j][k] >= 0) {
                        sum += myArr[i][j][k];
                        count++;
                    }
                }
            }
        }
        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    System.out.print(myArr[i][j][k] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }
        return "Среднее арифметическое для всех неотрицательных элементов: " + (float) sum / count;
    }

    //    Task 28
    //Отсортировать по возрастанию элементы трехмерного массива.
    static void task28(int[][][] myArr) {
        boolean sorted = false;
        while (!sorted) {
            sorted = true;
            int ix = 0;
            int jx = 0;
            int kx = 0;
            for (int i = 0; i < myArr.length; i++) {
                for (int j = 0; j < myArr[i].length; j++) {
                    for (int k = 0; k < myArr[i][j].length; k++) {
                        if (myArr[i][j][k] < myArr[ix][jx][kx]) {
                            int tmp = myArr[i][j][k];
                            myArr[i][j][k] = myArr[ix][jx][kx];
                            myArr[ix][jx][kx] = tmp;
                            sorted = false;
                        }
                        ix = i;
                        jx = j;
                        kx = k;
                    }
                }
            }
        }
        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    System.out.print(myArr[i][j][k] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    //    Task 29
    /*Отсортировать по возрастанию xy-плоскости трехмерного массива по сумме
    элементов в них.*/
    static void task29(int[][][] myArr) {
        boolean sorted = false;
        while (!sorted) {
            sorted = true;

            for (int i = 0; i < myArr.length - 1; i++) {
                int sum = 0;
                int sum2 = 0;
                for (int j = 0; j < myArr[i].length; j++) {
                    for (int k = 0; k < myArr[i][j].length; k++) {
                        sum += myArr[i][j][k];
                        sum2 += myArr[i + 1][j][k];
                    }
                }
                if (sum2 > sum) {
                    int[][] tmp;
                    tmp = myArr[i];
                    myArr[i] = myArr[i + 1];
                    myArr[i + 1] = tmp;
                    sorted = false;
                }
            }

        }
        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    System.out.print(myArr[i][j][k] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    //Task 30
    /*Последовательно вывести элементы трехмерного массива в строку через
    пробел.*/
    static void task30(int[][][] myArr) {
        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    System.out.print(myArr[i][j][k] + " ");
                }
            }
        }
    }

    //    Task 31
    /*Выписать в двумерный массив элементы xy-плоскости трехмерного массива,
    содержащей максимальный элемент в данном трехмерном массиве.*/
    static void task31(int[][][] myArr) {
        int[][] rewrite = myArr[0];
        for (int i = 0; i < myArr.length - 1; i++) {
            int max = 0;
            int max2 = 0;
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length - 1; k++) {
                    if (myArr[i][j][k] < myArr[i][j][k + 1]) {
                        max = myArr[i][j][k];
                    }
                    if (myArr[i + 1][j][k] < myArr[i + 1][j][k + 1]) {
                        max2 = myArr[i + 1][j][k];
                    }
                }
                if (max2 > max)
                    rewrite = myArr[i + 1];
                if (max > max2)
                    rewrite = myArr[i];
            }

        }

        for (int i = 0; i < rewrite.length; i++) {
            for (int j = 0; j < rewrite[i].length; j++) {
                System.out.print(rewrite[i][j] + "  ");
            }
            System.out.println();
        }
    }

    //    Task 32
    /*Последовательно вывести элементы xy-плоскостей трехмерного массива,
    перед выводом каждой из них указывать z-координату для выводимой плоскости
    (z = 0, z = 1 и т.д.)*/
    static void task32(int[][][] myArr) {
        for (int i = 0; i < myArr.length; i++) {
            System.out.println("Координата z = " + i);
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    System.out.print(myArr[i][j][k] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    //    Task 33
    /*Вычислить средние арифметические для элементов каждой из xy-плоскостей
    трехмерного массива и вывести их через запятую*/
    static void task33(int[][][] myArr) {

        for (int i = 0; i < myArr.length; i++) {
            int sum = 0;
            int count = 0;
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    sum += myArr[i][j][k];
                    count++;
                }
            }
            System.out.print((float) sum / count + " ");
        }
        System.out.println();
    }

    //    Task 34
    /*Отсортировать по возрастанию xy-плоскости трехмерного массива по
    возрастанию среднего арифметического элементов каждой из данных xy-
    плоскостей.*/
    static void task34(int[][][] myArr) {

        for (int i = 0; i < myArr.length - 1; i++) {
            int sum = 0;
            int sum2 = 0;
            int count = 0;
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    sum += myArr[i][j][k];
                    sum2 += myArr[i + 1][j][k];
                    count++;
                }
            }
            if ((float) sum2 / count > (float) sum / count) {
                int[][] tmp;
                tmp = myArr[i];
                myArr[i] = myArr[i + 1];
                myArr[i + 1] = tmp;
            }
        }

        for (int i = 0; i < myArr.length; i++) {
            for (int j = 0; j < myArr[i].length; j++) {
                for (int k = 0; k < myArr[i][j].length; k++) {
                    System.out.print(myArr[i][j][k] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }
}
