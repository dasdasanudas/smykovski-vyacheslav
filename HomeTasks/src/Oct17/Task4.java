package Oct17;

import java.util.Scanner;

/*
Задача 4.Сумма цифр числа
        Дано натуральное число N. Вычислите сумму его цифр.
        При решении этой задачи нельзя использовать строки, списки, массивы (ну и циклы, разумеется).
*/

public class Task4 {
    static int sum;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        System.out.println(recursion(x));
    }

    public static int recursion(int x) {
        if (x > 0) {
            if (x % 10 != 0) {
                sum += x % 10;
            }
            return recursion(x / 10);
        }
        return sum;
    }
}
