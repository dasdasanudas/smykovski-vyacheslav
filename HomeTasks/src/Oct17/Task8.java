package Oct17;

import java.util.Scanner;

/*
Задача 8. Палиндром
        Дано слово, состоящее только из строчных латинских букв. Проверьте, является ли это слово
        палиндромом. Выведите YES или NO.
        При решении этой задачи нельзя пользоваться циклами.
*/

public class Task8 {
    static String s;
    static String palindrome = "";
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        s = scanner.next();
        System.out.println(recursion(s));
    }

    public static String recursion(String s) {
        if (s.length() > 1) {
            if (s.charAt(0) == s.charAt(s.length() - 1)) {
                recursion(s.substring(1, s.length() - 1));
            } else {
                palindrome = "NO";
            }
        } else {
            palindrome = "YES";
        }
        return palindrome;
    }
}
