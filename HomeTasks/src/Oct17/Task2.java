package Oct17;

import java.util.Scanner;

/*
Задача 2.От A до B
        Даны два целых числа A и В (каждое в отдельной строке). Выведите все числа от A до B включительно, в
        порядке возрастания, если A < B, или в порядке убывания в противном случае.
        */

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        recursion(a, b);
    }

    public static void recursion(int a, int b) {
        if (a == b)
            System.out.println(b);
        if (a < b) {
            System.out.println(a);
            recursion(a + 1, b);
        }
        if (a > b) {
            recursion(a, b + 1);
            System.out.println(b);
        }
    }
}
