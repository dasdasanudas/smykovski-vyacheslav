package Oct17;

import java.util.Scanner;

/*
Задача 7.Разложение на множители
        Дано натуральное число n>1. Выведите все простые множители этого числа в порядке неубывания с
        учетом кратности. Алгоритм должен иметь сложность O(logn).
*/

public class Task7 {
    static int x;
    static int y = 2;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        x = scanner.nextInt();
        recursion(x, y);
    }

    public static void recursion(int x, int y) {
        if (y > x / 2) {
            System.out.print(x + " ");
        } else {
            if (x % y == 0) {
                System.out.print(y + " ");
                recursion(x / y, y);
            }
            if (x % y != 0) {
                recursion(x, y + 1);
            }
        }
    }
}
