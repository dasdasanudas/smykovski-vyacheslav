package Oct17;

import java.util.Scanner;

/*
Задача 3.Функция Аккермана
        В теории вычислимости важную роль играет функция Аккермана A(m,n), определенная следующим
        образом: даны два целых неотрицательных числа m и n, каждое в отдельной строке. Выведите A(m,n).
*/

public class Task3 {
    static int r = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        System.out.println(akkerman(m, n));
        System.out.println(r);

    }

    public static int akkerman(int m, int n) {
        int x = 0;
        r++;
        if (m == 0)
            x = n + 1;
        if (m > 0 && n == 0)
            x = akkerman(m - 1, 1);
        if (m > 0 && n > 0) {
            x = akkerman(m - 1, (akkerman(m, n - 1)));

        }
        return x;
    }
}
