package Oct17;

import java.util.Scanner;

/*
Задача 6. Цифры числа слева направо
        Дано натуральное число N. Выведите все его цифры по одной, в обычном порядке, разделяя их
        пробелами или новыми строками.
        При решении этой задачи нельзя использовать строки, списки, массивы (ну и циклы, разумеется).
        Разрешена только рекурсия и целочисленная арифметика.
*/

public class Task6 {
    static int x;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        x = scanner.nextInt();
        recursion(x);
    }

    public static void recursion(int x) {
        int s;
        if (x > 0) {
            s = x % 10;
            recursion(x / 10);
            System.out.print(s + " ");
        }
    }
}
