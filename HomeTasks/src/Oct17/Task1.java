package Oct17;

/*
Задача 1. От 1 до n
        Дано натуральное число n. Выведите все числа от 1 до n.
*/

public class Task1 {
    public static void main(String[] args) {
        int a = 20;
        recursion(a);
    }

    public static void recursion(int x) {
        if (1 < x) {
            recursion(x - 1);
        }
        System.out.println(x);
    }
}
