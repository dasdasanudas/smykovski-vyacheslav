package Oct17;

import java.util.Scanner;

/*
Задача 10. Максимум последовательности
        Дана последовательность натуральных чисел (одно число в строке), завершающаяся числом 0.
        Определите наибольшее значение числа в этой последовательности.
        В этой задаче нельзя использовать глобальные переменные и передавать какие-либо параметры в
        рекурсивную функцию. Функция получает данные, считывая их с клавиатуры. Функция возвращает
        единственное значение: максимум считанной последовательности. Гарантируется, что
        последовательность содержит хотя бы одно число (кроме нуля).
*/

public class Task10 {
    public static void main(String[] args) {
        System.out.println(recursion());
    }

    public static int recursion() {
        Scanner scanner = new Scanner(System.in);
        int max = scanner.nextInt();
        if (max == 0) {
            return Integer.MIN_VALUE;
        }
        int next = recursion();
        return max > next ? max : next;
    }
}

