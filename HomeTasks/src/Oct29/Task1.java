package Oct29;

import java.util.ArrayList;

/*
Задача. Создать абстрактный класс Фигура с абстрактным методом draw.
        Отнаследовать от него 5 типов фигур. Для каждого реализовать
        отрисовку этой фигуры в консоли.
        В main создать arraylist фигур. В цикле создать генератор фигур, который
        будет случайным образом выдавать фигуру и она будет записываться в
        имеющийся arraylist. В конце вывести фигуры на экран.
*/

public class Task1 {
    public static void main(String[] args) {
        ArrayList<Figure> figures = new ArrayList<>();
        figures.add(new Square());
        figures.add(new Parallelepiped());
        figures.add(new Triangle());
        figures.add(new Rhombus());
        figures.add(new Trapezium());

        for (int i = 0; i < 10; i++) {
            figures.get((int) (Math.random() * 4)).draw();
            System.out.println();
        }

    }
}

abstract class Figure {
    abstract void draw();
}

class Square extends Figure {

    char[][] arr = new char[9][9];

    @Override
    void draw() {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (j == arr[i].length - 1 || i == 0 || i == arr.length - 1 || j == 0) {
                    arr[i][j] = '*';
                } else
                    arr[i][j] = ' ';
                System.out.print(arr[i][j] + "  ");
            }
            System.out.println();
        }
    }
}

class Parallelepiped extends Figure {

    char[][] arr = new char[5][9];

    @Override
    void draw() {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (j == arr[i].length - 1 || i == 0 || i == arr.length - 1 || j == 0) {
                    arr[i][j] = '*';
                } else
                    arr[i][j] = ' ';
                System.out.print(arr[i][j] + "  ");
            }
            System.out.println();
        }
    }
}

class Triangle extends Figure {

    char[][] arr = new char[5][9];

    @Override
    void draw() {
        int z = 4;
        int k = 4;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (i == arr.length - 1 || j == z || j == k) {
                    arr[i][j] = '*';
                } else {
                    arr[i][j] = ' ';
                }
                System.out.print(arr[i][j] + "  ");
            }
            z++;
            k--;
            System.out.println();
        }
    }
}

class Rhombus extends Figure {

    char[][] arr = new char[9][9];

    @Override
    void draw() {
        int z = 4;
        int k = 4;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (j == z || j == k) {
                    arr[i][j] = '*';
                } else {
                    arr[i][j] = ' ';
                }
                System.out.print(arr[i][j] + "  ");
            }
            if (i < arr.length / 2) {
                z++;
                k--;
            } else {
                z--;
                k++;
            }
            System.out.println();
        }
    }
}

class Trapezium extends Figure {

    char[][] arr = new char[5][9];

    @Override
    void draw() {
        int z = 5;
        int k = 3;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (i == arr.length - 1 || i == 0 && j == arr[i].length / 2 ||  j == z || j == k) {
                    arr[i][j] = '*';
                } else {
                    arr[i][j] = ' ';
                }
                System.out.print(arr[i][j] + "  ");
            }
                z++;
                k--;
            System.out.println();
        }
    }
}

