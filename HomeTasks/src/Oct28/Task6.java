package Oct28;

import java.util.ArrayList;
import java.util.Scanner;

/*
Задача 6. Создать arraylist типа string и заполнить его 10 строками
        с клавиатуры. С помощью цикла найти самую маленькую или
        самую длинную строку, в зависимости от того, какая встретится
        первой
*/

public class Task6 {
    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            ArrayList<String> list = new ArrayList<>();
            int maxWidth = 0;
            int minWidth = Integer.MAX_VALUE;
            for (int i = 0; i < 10; i++) {
                list.add(scanner.next());
                if (list.get(i).length() < minWidth) {
                    minWidth = list.get(i).length();
                }
                if (list.get(i).length() > maxWidth) {
                    maxWidth = list.get(i).length();
                }
            }

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).length() == minWidth) {
                    System.out.println(list.get(i));
                    break;
                }
                if (list.get(i).length() == maxWidth) {
                    System.out.println(list.get(i));
                    break;
                }
            }
        }
    }
