package Oct28;

import java.util.ArrayList;
import java.util.Scanner;

/*
Задача 3.Создать arraylist типа string и заполнить его 10 строками с клавиатуры. С
        помощью цикла найти строку минимальной длины(или несколько, если таковые
        имеются) и вывести её(их) на экран.
*/

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> list = new ArrayList<>();
        int maxWidth = Integer.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            list.add(scanner.next());
            if (list.get(i).length() < maxWidth) {
                maxWidth = list.get(i).length();
            }
        }

        for (int i = 0; i < 10; i++) {
            if (list.get(i).length() == maxWidth)
                System.out.println(list.get(i));
        }
    }
}
