package Oct28;

import java.util.ArrayList;
import java.util.Scanner;

/*
Задача 4. Создать arraylist типа string произвольной длины. Добавлять в него строки.
        Но не в конец, а в начало списка.
*/

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> list = new ArrayList<>();
        while(true){
            String s = scanner.nextLine();
            if (s.isEmpty()) break;
            list.add(0, s);
        }
        for (String s : list) {
            System.out.println(s);
        }
    }

}
