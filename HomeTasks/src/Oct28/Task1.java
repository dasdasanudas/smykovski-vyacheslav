package Oct28;

import java.util.ArrayList;

/*
Задача 1. Создать arraylist типа string и заполнить его 10 строками. Вывести на экран
        содержимое списка.
*/

public class Task1 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add("This is the string N: " + (i + 1));
        }

        for (String s : list) {
            System.out.println(s);
        }
    }
}
