package Oct28;

import java.util.ArrayList;

/*
Задача 5. Создать arraylist типа string и заполнить его 10 строками. Произвести
        циклический сдвиг вправо 17 раз и вывести на экран.
        Пример:
        аааа
        сссс
        кккк
        рррр
        Сдвиг на 5 позиций:
        рррр
        аааа
        сссс
        кккк
*/

public class Task5 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add("This is the string N: " + (i + 1));
        }
        String[] arr = new String[list.size()];
        for (int i = 0; i < arr.length; i++) {
            int v = (17 % list.size() + i) % list.size();
            arr[(17 % list.size() + i ) % list.size()] = list.get(i);
        }

        for (String s : arr) {
            System.out.println(s);
        }
    }
}
