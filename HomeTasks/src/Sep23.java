public class Sep23 {

//Loops

    public static void main(String[] args) {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
// Task 01
        for (x = 0; x <= 10; x += 0.01) {
            maxTMP = Math.pow(x, 2);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 01: %10d\n", Math.round(maxRes));
// Task 02
        maxRes = 0;
        for (x = -15; x <= 15; x += 0.01) {
            maxTMP = Math.pow(x, 2) - x + 3;
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 02: %10d\n", Math.round(maxRes));
// Task 03
        maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            maxTMP = Math.sin(x) + Math.pow(x, 2);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 03: %10d\n", Math.round(maxRes));
// Task 04
        maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            maxTMP = Math.cos(x) + Math.pow(x, 2);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 04: %10d\n", Math.round(maxRes));
// Task 05
        maxRes = 0;
        for (x = -50; x <= 50; x += 0.01) {
            maxTMP = Math.pow(x, 3);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 05: %10d\n", Math.round(maxRes));
// Task 06
        maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            if (!(x >= -1 & x <= 1))
                maxTMP = 1 / Math.pow(x, 3);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 06: %10d\n", Math.round(maxRes));
// Task 07
        maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            if (!(x >= -1 & x <= 1))
                maxTMP = 1 / Math.pow(x, 3) + Math.pow(x, 3);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 07: %10d\n", Math.round(maxRes));
// Task 08
        maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            maxTMP = Math.pow(Math.E, x);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 08: %10d\n", Math.round(maxRes));
// Task 09
        maxRes = 0;
        for (x = -100; x <= 100; x += 0.01) {
            maxTMP = Math.pow(Math.E, Math.sin(x));
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 09: %10d\n", Math.round(maxRes));
// Task 10
        maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            maxTMP = Math.pow(x, Math.E);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 10: %10d\n", Math.round(maxRes));
// Task 11
        maxRes = 0;
        for (x = -5; x <= 5; x += 0.01) {
            maxTMP = Math.pow(x, Math.E) + x;
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 11: %10d\n", Math.round(maxRes));
// Task 12
        maxRes = 0;
        for (x = -5; x <= 5; x += 0.01) {
            maxTMP = Math.pow(x, Math.E) + Math.pow(x, 2);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.printf("Ответ к заданию 12: %10d\n", Math.round(maxRes));


    }
}
