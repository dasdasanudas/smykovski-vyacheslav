package Nov15;

import java.util.ArrayList;

 class Plague {
    static
    void start(ArrayList<Person> list) {
        for (int i = 0; i < 5; i++) {
            list.forEach(person -> person.setHp(person.getHp() - (int) (Math.random() * 35) + 1));
        }
        list.removeIf(person -> person.getHp() <= 0);
    }
}
