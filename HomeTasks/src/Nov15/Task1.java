package Nov15;

/*
Задача 1. Рыцарский турнир закончился! Пришла пора объявить победителей. Но безответственный
        клерк перепутал имена участников и теперь не может составить их правильный список. Нужно ему
        помочь. А именно, используя treeMap создать список участников с полями “Честь” и соответствующее
        ему “Имя рыцаря” и показать отсортированный список. Помним, чем больше чести, тем ближе к
        верхушке турнирной таблицы.
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

public class Task1 {
    public static void main(String[] args) throws IOException {
        TreeMap<Integer, String> map = new TreeMap<>();
        try {
            BufferedInputStream reader = new BufferedInputStream(new FileInputStream("src\\Nov15\\File"));
            StringBuilder tmp = new StringBuilder();
            int c;
            while ((c = reader.read()) != -1) {
                tmp.append((char) c);
            }
            ArrayList<String> names = new ArrayList<>(Arrays.asList(tmp.toString().split("\r\n")));
            map.put(2, names.get(0));
            map.put(4, names.get(1));
            map.put(5, names.get(2));
            map.put(3, names.get(3));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream("src\\Nov15\\Output"))) {
            for (Map.Entry<Integer, String> element : map.entrySet()) {
                writer.write((element.getKey() + " \t" + element.getValue() + "\n").getBytes());
                System.out.println(element.getKey() + " \t" + element.getValue());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
