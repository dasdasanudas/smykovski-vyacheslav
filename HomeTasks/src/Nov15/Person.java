package Nov15;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

abstract class Person {
    private int hp;
    private String name;

    public Person(int hp, String name) {
        this.hp = hp;
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Class: %8s \tName: %9s \tHP: %3s", getClass().getSimpleName(), name, hp);
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }
}

class King extends Person {
    public King(int hp, String name) {
        super(hp, name);
    }
}

class Noble extends Person {
    public Noble(int hp, String name) {
        super(hp, name);
    }
}

class Knight extends Person {
    public Knight(int hp, String name) {
        super(hp, name);
    }
}

class Peasant extends Person {
    public Peasant(int hp, String name) {
        super(hp, name);
    }
}

class Citizen {
    private ArrayList<Person> list = new ArrayList<>();

    ArrayList<Person> getSitizen() {
        try {
            BufferedInputStream reader = new BufferedInputStream(new FileInputStream("src\\Nov15\\Names"));
            StringBuilder tmp = new StringBuilder();
            int c;
            while ((c = reader.read()) != -1) {
                tmp.append((char) c);
            }
            ArrayList<String> names = new ArrayList<>(Arrays.asList(tmp.toString().split("\r\n")));
            for (int i = 1; i <= 25; i++) {
                if (i == 1) {
                    list.add(new King(100, names.get(i - 1)));
                }
                if (i <= 10) {
                    list.add(new Noble(85 + i, names.get(i)));
                }
                list.add(new Knight(70 + i, names.get(i + 10)));
                list.add(new Peasant(25 + i, names.get(i + 35)));
                list.add(new Peasant(25 + i, names.get(i + 60)));
                list.add(new Peasant(25 + i, names.get(i + 85)));
                list.add(new Peasant(25 + i, names.get(i + 110)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    void printAlive() {
        AtomicInteger countNoble = new AtomicInteger(0);
        AtomicInteger countKnight = new AtomicInteger(0);
        AtomicInteger countPeasant = new AtomicInteger(0);
        list.forEach(person -> {
            if (person instanceof Noble) {
                countNoble.incrementAndGet();
            }
            if (person instanceof Knight) {
                countKnight.incrementAndGet();
            }
            if (person instanceof Peasant) {
                countPeasant.incrementAndGet();
            }
        });

        if (list.get(0) instanceof King) {
            System.out.println("King is alive!");
        } else {
            System.out.println("King is dead!");
        }
        System.out.println(countNoble + " nobles are alive.\n" +
                countKnight + " knights are alive.\n" +
                countPeasant + " peasants are alive.\n");

        list.forEach(System.out::println);

        try (BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream("src\\Nov15\\Output"))) {
            for (Person person : list) {
                writer.write(person.toString().getBytes());
                writer.write("\n".getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
