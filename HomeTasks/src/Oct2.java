import java.util.Arrays;

/*
1) Создать класс “Human” с двумя строковыми переменными для имени и фамилии,
вещественной переменной для возраста и логической переменной для пола.
2) Создать класс “Monster” со строковой переменной для имени, целочисленной переменной
для очков здоровья, двумя вещественными переменными для силы атаки и показателя
защиты
3)Создать класс с двумя переменными. Добавить функцию вывода на экран и функцию
изменения этих переменных. Добавить функцию, которая находит сумму значений этих
переменных, и функцию которая находит наибольшее значение из этих двух переменных.
4)Создайте структуру с именем student, содержащую поля: фамилия и инициалы, номер
группы, успеваемость (массив из пяти элементов). Создать массив из десяти элементов
такого типа, упорядочить записи по возрастанию среднего балла. Добавить возможность
вывода фамилий и номеров групп студентов, имеющих оценки, равные только 4 или 5.
*/

public class Oct2 {
    public static void main(String[] args) {
        Third third = new Third(80.5, 100.0);
        third.print();
        System.out.println("Сумма значений: " + third.sum());
        System.out.println("Наибольшее значение: " + third.max());

        Student[] student = new Student[10];
        System.out.println("\n\nЗадание 4.\nСписок студентов:");
        for (int i = 0; i < student.length; i++) {
            int[] arr = new int[5];
            for (int j = 0; j < arr.length; j++) {
                arr[j] = (int) (Math.random() * 5 + 1);
            }
            student[i] = new Student("John Doe" + i, 1 + i, arr);
            System.out.print(student[i].print());
        }
        for (int i = 0; i < student.length; i++) {
            for (int j = 0; j < student.length - 1 - i; j++) {
                if (student[j].avg < student[j + 1].avg) {
                    Student temp = student[j];
                    student[j] = student[j + 1];
                    student[j + 1] = temp;
                }
            }
        }

        System.out.println("\n\nСортировка по успеваемости\n");
        for (int i = 0; i < student.length; i++) {
            System.out.print(student[i].print());
        }

        printExcellent(student);
    }
    static void printExcellent(Student[] students) {

        for (var student: students) {
            int count = 0;
            for (int i = 0; i < student.progress.length; i++) {
                if (student.progress[i] > 3)
                    count++;
            }
            if (count == student.progress.length)
                System.out.printf("\nХорошие ученики:\nФИО: %s\tГруппа: %s\n", student.lastNameVsInits, student.groupNum);
        }
    }
}

//        Task 01
class Human {
    String firstName;
    String lastName;
    double age;
    boolean manSex;
}

//        Task 02
class Monster {
    String name;
    int health;
    int attack;
    int defence;
}

//        Task 03
class Third {
    double attack;
    double defence;

    Third(double attack, double defence) {
        this.attack = attack;
        this.defence = defence;
    }

    void print() {
        System.out.printf("Задание 03.\t attack: %s\t defence: %s\n", attack, defence);
    }

    double sum() {
        return attack + defence;
    }

    double max() {
        if (attack > defence)
            return attack;
        else
            return defence;
    }
}

//        Task 04
class Student {
    String lastNameVsInits;
    int groupNum;
    int[] progress = new int[5];
    double avg;

    public Student(String lastNameVsInits, int groupNum, int[] progress) {
        this.lastNameVsInits = lastNameVsInits;
        this.groupNum = groupNum;
        this.progress = progress;
        this.avg = 0.0;
        int sum = 0;
        for (int element : progress) {
            sum += element;
        }
        avg = (double) sum / progress.length;
    }

    String print() {
        return String.format("ФИО: %s\tГруппа: %s\t Оценки: %s\n", lastNameVsInits, groupNum, Arrays.toString(progress));
    }

}
