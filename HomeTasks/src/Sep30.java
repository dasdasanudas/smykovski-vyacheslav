public class Sep30 {

    public static void main(String[] args) {
        double[][] myArrayD = {{1, 5, 9}, {6, 8, -5}, {10, -1, 7}};
        System.out.println("Наш двумерный массив: ");
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                System.out.print(myArrayD[i][j] + " ");
            }
            System.out.println();
        }

        double[] arr = new double[myArrayD.length * myArrayD[0].length];
        int ind = 0;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                arr[ind] = myArrayD[i][j];
                ind++;
            }
        }

        int[][] myArrayM = new int[3][3];
        System.out.println("Наш двумерный массив для умножения: ");
        for (int i = 0; i < myArrayM.length; i++) {
            for (int j = 0; j < myArrayM[i].length; j++) {
                myArrayM[i][j] = ((int) (Math.random() * 12) - 4);
                System.out.print(myArrayM[i][j] + (j == myArrayM.length - 1 ? "\n" : "     "));
            }
        }
        int[] myArrayS = new int[myArrayD.length * myArrayM.length];

        System.out.println("Задание 1 (отсортировать по возрастанию):");
        task1(myArrayD, arr);
        System.out.println("Задание 2 (отсортировать по убыванию):");
        task2(myArrayD, arr);
        System.out.println("Задание 3 (отсортировать столбцы по возрастанию суммы):");
        task3(myArrayD);
        System.out.println("Задание 4 (отсортировать столбцы по убыванию суммы ):");
        task4(myArrayD);
        System.out.println("Задание 5 (отсортировать строки по возрастанию суммы четных элементов):");
        task5(myArrayD);
        System.out.println("Задание 6 (отсортировать строки по убыванию суммы четных элементов):");
        task6(myArrayD);
        task7(myArrayD);
        task8(myArrayD);
        System.out.println("Задание 9 (умножение двух квадратных матриц):");
        task9(myArrayD, myArrayM, myArrayS);
    }

    // Task 01
    static void task1(double[][] myArrayD, double[] arr) {
        double rezTmp;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    rezTmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = rezTmp;
                }
            }
        }
        int ind = 0;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                myArrayD[i][j] = arr[ind];
                System.out.print(myArrayD[i][j] + "  ");
                ind++;
            }
            System.out.println();
        }
    }

    // Task 02
    static void task2(double[][] myArrayD, double[] arr) {
        double rezTmp;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] < arr[j + 1]) {
                    rezTmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = rezTmp;
                }
            }
        }
        int ind = 0;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                myArrayD[i][j] = arr[ind];
                System.out.print(myArrayD[i][j] + "  ");
                ind++;
            }
            System.out.println();
        }
    }

    // Task 03
    static void task3(double[][] myArrayD) {
        double sum;
        double replace;
        int replaceInd;
        double[] arr = new double[myArrayD[0].length];
        int[] arrInd = new int[myArrayD[0].length];
        for (int j = 0; j < myArrayD[0].length; j++) {
            sum = 0.0;
            for (int i = 0; i < myArrayD[j].length; i++) {
                sum += myArrayD[i][j];
            }
            arr[j] = sum;
            arrInd[j] = j;
        }
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    replace = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = replace;
                    replaceInd = arrInd[j];
                    arrInd[j] = arrInd[j + 1];
                    arrInd[j + 1] = replaceInd;
                }
            }
        }
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < arrInd.length; j++) {
                System.out.print(myArrayD[i][arrInd[j]] + "  ");
            }
            System.out.println();
        }
    }

    // Task 04
    static void task4(double[][] myArrayD) {
        double sum;
        double replace;
        int replaceInd;
        double[] arr = new double[myArrayD[0].length];
        int[] arrInd = new int[myArrayD[0].length];
        for (int j = 0; j < myArrayD[0].length; j++) {
            sum = 0.0;
            for (int i = 0; i < myArrayD[j].length; i++) {
                sum += myArrayD[i][j];
            }
            arr[j] = sum;
            arrInd[j] = j;
        }
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] < arr[j + 1]) {
                    replace = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = replace;
                    replaceInd = arrInd[j];
                    arrInd[j] = arrInd[j + 1];
                    arrInd[j + 1] = replaceInd;
                }
            }
        }
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < arrInd.length; j++) {
                System.out.print(myArrayD[i][arrInd[j]] + "  ");
            }
            System.out.println();
        }
    }

    // Task 05
    static void task5(double[][] myArrayD) {
        double sum;
        double replace;
        int replaceInd;
        double[] arr = new double[myArrayD[0].length];
        int[] arrInd = new int[myArrayD[0].length];
        for (int i = 0; i < myArrayD.length; i++) {
            sum = 0.0;
            for (int j = 0; j < myArrayD[i].length; j += 2) {
                sum += myArrayD[i][j];
            }
            arr[i] = sum;
            arrInd[i] = i;
        }
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    replace = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = replace;
                    replaceInd = arrInd[j];
                    arrInd[j] = arrInd[j + 1];
                    arrInd[j + 1] = replaceInd;
                }
            }
        }
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < arrInd.length; j++) {
                System.out.print(myArrayD[arrInd[i]][j] + "  ");
            }
            System.out.println();
        }
    }

    // Task 06
    static void task6(double[][] myArrayD) {
        double sum;
        double replace;
        int replaceInd;
        double[] arr = new double[myArrayD[0].length];
        int[] arrInd = new int[myArrayD[0].length];
        for (int i = 0; i < myArrayD.length; i++) {
            sum = 0.0;
            for (int j = 0; j < myArrayD[i].length; j += 2) {
                sum += myArrayD[i][j];
            }
            arr[i] = sum;
            arrInd[i] = i;
        }
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] < arr[j + 1]) {
                    replace = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = replace;
                    replaceInd = arrInd[j];
                    arrInd[j] = arrInd[j + 1];
                    arrInd[j + 1] = replaceInd;
                }
            }
        }
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < arrInd.length; j++) {
                System.out.print(myArrayD[arrInd[i]][j] + "  ");
            }
            System.out.println();
        }
    }

    // Task 07
    static void task7(double[][] myArrayD) {
        int ind = 0;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                int col = 2;
                double result = 0.0;
                for (int del = 2; del <= myArrayD[i][j]; del++) {
                    result = myArrayD[i][j] % del;
                    if (result == 0 && del != myArrayD[i][j]) {
                        col++;
                    }
                }
                if (col < 3) {
                    ind++;
                    break;
                }
            }
        }
        System.out.println("Задание 7 (найти количество строк, содержащих простые числа): " + ind);
    }

    // Task 08
    static void task8(double[][] myArrayD) {
        int ind = 0;
        for (int j = 0; j < myArrayD[0].length; j++) {
            for (int i = 0; i < myArrayD.length; i++) {
                int col = 2;
                double result = 0.0;
                for (int del = 2; del <= myArrayD[i][j]; del++) {
                    result = myArrayD[i][j] % del;
                    if (result == 0 && del != myArrayD[i][j]) {
                        col++;
                    }
                }
                if (col < 3) {
                    ind++;
                    break;
                }
            }
        }
        System.out.println("Задание 8 (найти количество столбцов, содержащих простые числа): " + ind);
    }

    // Task 09
    static void task9(double[][] myArrayD, int[][] myArrayM, int[] myArrayS) {
        int ind = 0;
        for (int j = 0; j < myArrayD[0].length; j++) {
            for (int i = 0; i < myArrayD.length; i++) {
                int sum = 0;
                for (int k = 0; k < myArrayD.length; k++) {
                    sum += myArrayM[j][k] * myArrayD[k][i];
                }
                myArrayS[ind] = sum;
                ind++;
            }
        }

        ind = 0;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                myArrayD[i][j] = myArrayS[ind];
                System.out.print(myArrayD[i][j] + "  ");
                ind++;
            }
            System.out.println();
        }
    }
}
