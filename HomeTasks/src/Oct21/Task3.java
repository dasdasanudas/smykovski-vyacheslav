package Oct21;

import java.util.Scanner;

/*
Задача 3. Написать программу, которая будет считать
        значение функции f(x) = 18*x ^ 2 + (54/x) - 8 на заданном
        промежутке ( промежуток вводится с клавиатуры ). Далее
        вводится с клавиатуры значение при котором нужно посчитать
        функцию. Если значение не входит в заданный промежуток -
        бросается исключение. Программа при этом не должна
        завершать своей работы.
*/

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        function(x, y);
    }
    static void function(int x, int y){
        Scanner scanner = new Scanner(System.in);
        int z = scanner.nextInt();
        try {
            if (z < x || z > y) throw new Exception("Недопустимое значение");
            double result = 18 * Math.pow(x, 2) + 54.0 / x;
            System.out.println(result);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
