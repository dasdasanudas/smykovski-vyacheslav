package Oct21;

import java.util.Scanner;

/*Задача 2. Написать программу, которая будет считать значение функции f(x) = 12*x + 44 на
        промежутке [-71; 14]. Значение при котором нужно посчитать значение функции вводится с
        клавиатуры. Если оно не входит в заданный промежуток - бросается исключение. Программа при
        этом не должна завершать своей работы.*/

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        function(x);
    }

    public static void function(int x) {
        try {
            if (x < -71 || x > 14) throw new OutOfRulesException("Недопустимое значение");
            int result = 12 * x + 44;
            System.out.println(result);
        } catch (OutOfRulesException ex) {
            System.out.println(ex.getMessage());
        }
    }
}

class OutOfRulesException extends Exception {
    public OutOfRulesException(String s) {
        super(s);
    }
}
