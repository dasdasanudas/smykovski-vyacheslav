package Oct21;

import java.util.Scanner;

/*
Задача 1. Написать программу, которая будет считать значение функции f(x) = 1 / x при заданных
        значениях. Программа может принимать на вход любые значения отличные от нуля. Поймать в ней
        случай деления на ноль с помощью exeption’ов так, чтобы программа не завершала свою работу.
*/

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        function(x);
    }

    public static void function(int x) {
        int r = 0;
        try {
            r = 1 / x;
        } catch (Exception ex) {
            System.out.println("Нельзя делить на ноль");
        } finally {
            System.out.println(r);
        }
    }
}

