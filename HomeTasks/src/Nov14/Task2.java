package Nov14;

/*
Создать класс маг с полями количество здоровье, количество магии и имя. Написать
компаратор для сравнения этих двух магов по количеству наносимого урона. Составить список
самых сильных магов с использованием treeSet.
*/

import java.util.Comparator;
import java.util.TreeSet;

public class Task2 {
    public static void main(String[] args) {
        Comparator<Wizard> comparator = new MagAttackComparator();
        TreeSet<Wizard> set = new TreeSet(comparator);
        set.add(new Wizard(1600, 300, "Sorcerer"));
        set.add(new Wizard(1800, 270, "Necromancer"));
        set.add(new Wizard(1500, 290, "Spell Singer"));
        set.add(new Wizard(1400, 310, "Spell Howler"));

        for (Wizard wizard : set) {
            System.out.println(wizard);
        }
    }
}
class Wizard {
    private int hp;
    private int magAttack;
    private String name;

    public Wizard(int hp, int magAttack, String name) {
        this.hp = hp;
        this.magAttack = magAttack;
        this.name = name;
    }

    public int getMagAttack() {
        return magAttack;
    }

    @Override
    public String toString() {
        return "HP: " + hp + " \tAttack: " + magAttack + " \tName: " + name;
    }
}
class MagAttackComparator implements Comparator<Wizard> {

    @Override
    public int compare(Wizard a, Wizard b) {
        return Integer.compare(a.getMagAttack(), b.getMagAttack());
    }
}
