package Nov14;

/*
Создать класс воин с полями количество здоровье, количество урона и имя. Написать
компаратор для сравнения этих двух воинов по количеству наносимого урона. Найти самого
слабого и самого сильного рыцаря из списка 10 рыцарей используя treeSet.
*/

import java.util.Comparator;
import java.util.TreeSet;

public class Task1 {
    public static void main(String[] args) {
        Comparator<Warrior> comparator = new WarriorAttackComparator();
        TreeSet<Warrior> set = new TreeSet(comparator);
        set.add(new Warrior(2400, 190, "Hawk Eye"));
        set.add(new Warrior(1800, 200, "Phantom Ranger"));
        set.add(new Warrior(2100, 180, "Silver Ranger"));
        set.add(new Warrior(2400, 120, "Treasure Hunter"));
        set.add(new Warrior(2100, 110, "Plains Walker"));
        set.add(new Warrior(1900, 130, "Abyss Walker"));
        set.add(new Warrior(4600, 150, "Gladiator"));
        set.add(new Warrior(5600, 250, "Destroyer"));
        set.add(new Warrior(4400, 160, "Tyrant"));
        set.add(new Warrior(5000, 100, "Warlord"));

        for (Warrior warrior : set) {
            System.out.println(warrior);
        }
    }
}

class Warrior {
    private int hp;
    private int physAttack;
    private String name;

    public Warrior(int hp, int physAttack, String name) {
        this.hp = hp;
        this.physAttack = physAttack;
        this.name = name;
    }

    public int getPhysAttack() {
        return physAttack;
    }

    @Override
    public String toString() {
        return "HP: " + hp + " \tAttack: " + physAttack + " \tName: " + name;
    }
}

class WarriorAttackComparator implements Comparator<Warrior> {

    @Override
    public int compare(Warrior a, Warrior b) {
        return Integer.compare(a.getPhysAttack(), b.getPhysAttack());
    }
}
