package Nov14;

import java.util.Comparator;
import java.util.TreeSet;

public class Task3 {
    public static void main(String[] args) {
        Comparator<Knight> comparator = new KnightHPComparator().thenComparing(new KnightArmorComparator()).
                thenComparing(new KnightAttackComparator());
        TreeSet<Knight> set = new TreeSet(comparator);
        set.add(new Knight(2400, 2000, 70, "Paladin"));
        set.add(new Knight(1800, 1800, 80, "Dark Avenger"));
        set.add(new Knight(2100, 2200, 60, "Temple Knight"));
        set.add(new Knight(2400, 1600, 90, "Shillien Knight"));

        for (Knight knight : set) {
            System.out.println(knight);
        }
    }
}

class Knight {
    private int hp;
    private int armor;
    private int physAttack;
    private String name;


    public Knight(int hp, int armor, int physAttack, String name) {
        this.hp = hp;
        this.armor = armor;
        this.physAttack = physAttack;
        this.name = name;
    }

    @Override
    public String toString() {
        return "HP: " + hp + " \tArmor: " + armor + " \tAttack: " + physAttack + " \tName: " + name;
    }

    public int getHp() {
        return hp;
    }

    public int getArmor() {
        return armor;
    }

    public int getPhysAttack() {
        return physAttack;
    }
}

class KnightAttackComparator implements Comparator<Knight> {
    @Override
    public int compare(Knight a, Knight b) {
        return Integer.compare(a.getPhysAttack(), b.getPhysAttack());
    }
}

class KnightHPComparator implements Comparator<Knight> {
    @Override
    public int compare(Knight a, Knight b) {
        return Integer.compare(a.getHp(), b.getHp());
    }
}

class KnightArmorComparator implements Comparator<Knight> {
    @Override
    public int compare(Knight a, Knight b) {
        return Integer.compare(a.getArmor(), b.getArmor());
    }
}


