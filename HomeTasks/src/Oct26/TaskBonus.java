package Oct26;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/*
Задача (^-^). Построить три класса (базовый и 3 потомка), описывающих некоторых хищных животных
        (один из потомков), всеядных(второй потомок) и травоядных (третий потомок). Описать в базовом классе
        абстрактный метод для расчета количества и типа пищи, необходимого для пропитания животного в
        зоопарке.
        a) Упорядочить всю последовательность животных по убыванию количества пищи. При совпадении
        значений – упорядочивать данные по алфавиту по имени. Вывести идентификатор животного, имя, тип и
        количество потребляемой пищи для всех элементов списка.
        b) Вывести первые 5 имен животных из полученного в пункте а) списка.
        c) Вывести последние 3 идентификатора животных из полученного в пункте а) списка.
        d) Организовать запись и чтение коллекции в/из файл.
        e) Организовать обработку некорректного формата входного файла.
*/

public class TaskBonus {
    public static void main(String[] args) throws IOException {
        ArrayList<Animal> animals = new ArrayList<>();
        animals.add(new Herbivore("Elephant", 30000));
        animals.add(new Omnivore("Pig", 300));
        animals.add(new Omnivore("Bear", 5000));
        animals.add(new Carnivore("Lion", 10000));
        animals.add(new Carnivore("Tiger", 7000));

        for (int i = 0; i < animals.size() - 1; i++) {
            for (int j = 0; j < animals.size() - i - 1; j++) {
                if (animals.get(j).metabolism() < animals.get(j + 1).metabolism()) {
                    animals.add(animals.get(j));
                    animals.remove(j);
                    j--;
                }
            }

        }

        for (int i = 0; i < 5; i++) {
            System.out.println("ID: " + i + "\tName: " + animals.get(i).getName()+ "\tType: " + animals.get(i).getClass().getSimpleName() + "\tFood count: " + animals.get(i).metabolism());
        }
        System.out.println();


        for (int i = animals.size() - 1; i > animals.size() - 4; i--) {
            System.out.println("ID: " + i + "\tName: " + animals.get(i).getName()+ "\tType: " + animals.get(i).getClass().getSimpleName() + "\tFood count: " + animals.get(i).metabolism());
        }
        System.out.println();

        try {
            FileWriter writer = new FileWriter("C:/Users/dasbr/IdeaProjects/HomeTasks/src/Oct26/MetabolismFile");
            for (Animal animal : animals) {
                writer.write("Name: " + animal.getName()+ "\tType: " + animal.getClass().getSimpleName() + "\tFood count: " + animal.metabolism());
                writer.write(System.lineSeparator());
            }
            writer.close();
        } catch (IOException e) {
            System.out.println("File not found!");
        }
        try {Files.lines(Paths.get("C:/Users/dasbr/IdeaProjects/HomeTasks/src/Oct26/MetabolismFile"), StandardCharsets.UTF_8).forEach(System.out::println);
        } catch (Exception e){
            System.out.println("File not found!");
        }
    }
}

abstract class Animal {
    private String name;
    private int needFood;

    abstract int metabolism();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNeedFood() {
        return needFood;
    }

    public void setNeedFood(int needFood) {
        this.needFood = needFood;
    }

    @Override
    public String toString() {
        return name + "\t" + needFood;
    }
}

class Herbivore extends Animal {

    public Herbivore(String name, int needFood) {
        super.setName(name);
        super.setNeedFood(needFood);
    }

    @Override
    int metabolism() {
        return super.getNeedFood();
    }

    @Override
    public String getName() {
        return super.getName();
    }
}

class Omnivore extends Animal {

    public Omnivore(String name, int needFood) {
        super.setName(name);
        super.setNeedFood(needFood);
    }

    @Override
    int metabolism() {
        return super.getNeedFood();
    }

    @Override
    public String getName() {
        return super.getName();
    }
}

class Carnivore extends Animal {

    public Carnivore(String name, int needFood) {
        super.setName(name);
        super.setNeedFood(needFood);
    }

    @Override
    int metabolism() {
        return super.getNeedFood();
    }

    @Override
    public String getName() {
        return super.getName();
    }
}