package Oct26;

/*
Задача 5.Создать квадратную матрицу, на диагонали которой находятся тройки, выше диагонали
        находятся двойки, остальные элементы равна единице.
*/

public class Task5 {
    public static void main(String[] args) {
        int[][] matrix = new int[5][5];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i == j)
                    matrix[i][j] = 3;
                if (j > i)
                    matrix[i][j] = 2;
                if (j < i)
                    matrix[i][j] = 1;
                System.out.printf("%3d", matrix[i][j]);
            }
            System.out.println();
            }
    }
}
