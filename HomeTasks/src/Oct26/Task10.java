package Oct26;

import java.util.ArrayList;

/*
Задача 10. Класс Абонент: Идентификационный номер, Фамилия, Имя, Отчество, Адрес, Номер
        кредитной карточки, Дебет, Кредит, Время междугородных и городских переговоров; Конструктор;
        Методы: установка значений атрибутов, получение значений атрибутов, вывод информации.
        Создать массив объектов данного класса. Вывести сведения относительно абонентов, у которых
        время городских переговоров превышает заданное. Сведения относительно абонентов, которые
        пользовались междугородной связью.
*/

public class Task10 {
    public static void main(String[] args) {
        ArrayList<Abonent> database = new ArrayList<>();
        database.add(new Abonent(16108, "Khvost", "Irina", "Alexandrovna", "Penyazkova 37, 40", 427600003, 340, 20, 0, 320));
        database.add(new Abonent(16109, "Smykovski", "Slava", "Anatolyevich", "Penyazkova 37, 40", 427620004, 1050, 70, 70, 40));
        database.add(new Abonent(16110, "Ronzhin", "Misha", "Andreevich", "SPB", 423720004, 750, 35, 90, 470));

        int limitOfLocalTalks = 300;
        for (Abonent abonent : database) {
            if (abonent.getDurationOfLocalTalks() > limitOfLocalTalks) {
                abonent.print();
            }
        }

        for (Abonent abonent : database) {
            if (abonent.getDurationOfInternationalTalks() > 0) {
                abonent.print();
            }
        }
    }
}

class Abonent {
    private int idNumber;
    private String lastName;
    private String name;
    private String fathersName;
    private String adress;
    private int creditCardNumber;
    private int debet;
    private int credit;
    private int durationOfInternationalTalks;
    private int durationOfLocalTalks;


    public int getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(int idNumber) {
        this.idNumber = idNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFathersName() {
        return fathersName;
    }

    public void setFathersName(String fathersName) {
        this.fathersName = fathersName;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public int getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(int creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public int getDebet() {
        return debet;
    }

    public void setDebet(int debet) {
        this.debet = debet;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public int getDurationOfInternationalTalks() {
        return durationOfInternationalTalks;
    }

    public void setDurationOfInternationalTalks(int durationOfInternationalTalks) {
        this.durationOfInternationalTalks = durationOfInternationalTalks;
    }

    public int getDurationOfLocalTalks() {
        return durationOfLocalTalks;
    }

    public void setDurationOfLocalTalks(int durationOfLocalTalks) {
        this.durationOfLocalTalks = durationOfLocalTalks;
    }

    public Abonent(int idNumber, String lastName, String name, String fathersName, String adress, int creditCardNumber, int debet, int credit, int durationOfInternationalTalks, int durationOfLocalTalks) {
        this.idNumber = idNumber;
        this.lastName = lastName;
        this.name = name;
        this.fathersName = fathersName;
        this.adress = adress;
        this.creditCardNumber = creditCardNumber;
        this.debet = debet;
        this.credit = credit;
        this.durationOfInternationalTalks = durationOfInternationalTalks;
        this.durationOfLocalTalks = durationOfLocalTalks;
    }

    public void print() {
        System.out.println("Идентификационный номер: " + getIdNumber() +
                "\nФамилия: " + getLastName() + "\tИмя: " + getName() + "\tОтчество: " + getFathersName() +
                "\nАдрес: " + getAdress() + "\nНомер кредитной карточки: " + getCreditCardNumber() +
                "\tДебет: " + getDebet() + "\tКредит: " + getDebet() +
                "\nВремя междугородных переговоров: " + getDurationOfInternationalTalks() +
                "\nВремя городских переговоров: " + getDurationOfLocalTalks());
    }
}
