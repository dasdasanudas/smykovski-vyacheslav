package Oct26;

/*
Задача 8. Описать класс, представляющий треугольник. Предусмотреть методы для создания
        объектов, вычисления площади, периметра и точки пересечения медиан.
*/

public class Task8 {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(0, 0, 0, 3, 4, 3);
        System.out.println("Length of the triangle side AB: " + triangle.getSideAB() +
                ", BC: " + triangle.getSideBC() + ", CA: " + triangle.getSideAC());
        System.out.println("Perimeter of the triangle: " + triangle.perimeter());
        System.out.println("Square of the triangle by Heron's formula: " + triangle.square());
        System.out.println("The coordinate of medians interceptions : " + triangle.centroid().toString());
    }
}

class Triangle {
    private int x1, x2, x3, y1, y2, y3;
    private double sideAB;
    private double sideBC;
    private double sideAC;

    class Dot {
        private int x;
        private int y;

        Dot(int x, int y) {
            this.x = x;
            this.y = y;
        }

        int getX() {
            return x;
        }

        int getY() {
            return y;
        }

        @Override
        public String toString() {
            return "x: " + getX() + "\t y: " + getY();
        }
    }

    public Triangle(int x1, int y1, int x2, int y2, int x3, int y3) {
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
        this.y1 = y1;
        this.y2 = y2;
        this.y3 = y3;
        sideAB = Math.sqrt(Math.pow((y2 - y1), 2) + Math.pow((x2 - x1), 2));
        sideBC = Math.sqrt(Math.pow((y3 - y2), 2) + Math.pow((x3 - x2), 2));
        sideAC = Math.sqrt(Math.pow((y3 - y1), 2) + Math.pow((x3 - x1), 2));

        if (sideAB + sideBC <= sideAC || sideBC + sideAC <= sideAB || sideAB + sideAC <= sideBC)
            System.out.println("Warning! There is no such triangle!");
    }

    public double perimeter() {
        return sideAB + sideBC + sideAC;
    }

    public double square() {
        return Math.sqrt(perimeter() / 2 * (perimeter() / 2 - sideAB) * (perimeter() / 2 - sideBC) * (perimeter() / 2 - sideAC));
    }

    public Dot centroid() {
        Dot mediansIntersection = new Dot((x1 + x2 + x3) / 3, (y1 + y2 + y3) / 3);
        return mediansIntersection;
    }

    public double getSideAB() {
        return sideAB;
    }

    public double getSideBC() {
        return sideBC;
    }

    public double getSideAC() {
        return sideAC;
    }
}
