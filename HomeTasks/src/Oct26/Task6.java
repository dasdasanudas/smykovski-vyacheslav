package Oct26;

/*
Задача 6. Сформировать квадратную матрицу n x n, на диагонали которой находятся случайные
        числа из диапазона [1; 9], а остальные числа равны 1.
*/

public class Task6 {
    public static void main(String[] args) {
        int[][] matrix = new int[5][5];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i == j)
                    matrix[i][j] = (int)(Math.random() * 9) + 1;
                else
                    matrix[i][j] = 1;
                System.out.printf("%3d", matrix[i][j]);
            }
            System.out.println();
        }
    }
}
