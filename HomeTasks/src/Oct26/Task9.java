package Oct26;

import java.util.Scanner;

/*
Задача 9. Составить описание класса для представления времени. Предусмотреть возможности
        установки времени и изменения его отдельных полей (час, минута, секунда) с проверкой
        допустимости вводимых значений. В случае недопустимых значений полей выбрасываются
        исключения. Создать методы изменения времени на заданное количество часов, минут и секунд
*/

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What's the time?");
        System.out.println("Hours: ");
        int hour = scanner.nextInt();
        System.out.println("Minutes: ");
        int min = scanner.nextInt();
        System.out.println("Seconds: ");
        int sec = scanner.nextInt();

        Time time = new Time(hour, min, sec);

        System.out.println("Add the time!");
        System.out.println("Hours: ");
        hour = scanner.nextInt();
        System.out.println("Minutes: ");
        min = scanner.nextInt();
        System.out.println("Seconds: ");
        sec = scanner.nextInt();
        time.add(hour, min, sec);
        System.out.println(time.print());
    }
}

class Time {
    private int hour;
    private int min;
    private int sec;


    public Time(int hour, int min, int sec) {
        setHour(hour);
        setMin(min);
        setSec(sec);
    }

    public void setHour(int hour) {
        if (hour >= 0 && hour <= 23)
            this.hour = hour;
        else
            System.out.println("Error! 0 - 23 available");
    }

    public void setMin(int min) {
        if (min >= 0 && min <= 59)
        this.min = min;
        else
            System.out.println("Error! 0 - 59 available");
    }

    public void setSec(int sec) {
        if (sec >= 0 && sec <= 59)
        this.sec = sec;
        else
            System.out.println("Error! 0 - 59 available");
    }

    public void add(int hour, int min, int sec) {
        if (sec >= 0 || min >= 0 || hour >= 0){
            this.hour += hour;
            this.min += min;
            this.sec += sec;
        }
        change();
    }

    void change() {
        int time = sec + (min * 60) + (hour * 60 * 60);
        this.hour = time / 60 / 60;
        this.min = (time - (hour * 60 * 60)) / 60;
        this.sec = time - (hour * 60 * 60) - (min * 60);
        this.hour = hour % 24;
    }

    public String print() {
        return String.format("%d:%02d:%02d", hour, min, sec);
    }
}