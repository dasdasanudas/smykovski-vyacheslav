package Oct26;

import java.util.Scanner;

/*
Задача 1. Компьютер загадывает число от 1 до n. У пользователя k попыток отгадать. После каждой
        неудачной попытки компьютер сообщает меньше или больше загаданное число. В конце игры текст
        с результатом (или “Вы угадали”, или “Попытки закончились”).
*/

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите количество попыток: ");
        int k = sc.nextInt();
        System.out.print("Введите max возможное значение: ");
        int n = sc.nextInt();
        Computer computer = new Computer();
        computer.setNum(n);
        int numComp = computer.getNum();
        for (int i = 0; i < k; i++) {
            System.out.print("Задайте число от 1 до " + n + ": ");
            int numUser = sc.nextInt();
            if (numUser == numComp) {
                System.out.println("Вы угадали!");
                break;
            }
            if (numUser < numComp) {
                System.out.println("Ваше число меньше загадоного.");
            } else {
                System.out.println("Ваше число больше загадоного.");
            }
            if (i == k - 1)
                System.out.println("Попытки закончились.\nЧисло компьютера: " + numComp);
        }
    }
}

class Computer {
    private int numComp;

    Computer() {
    }

    int getNum() {
        return numComp;
    }

    void setNum(int n) {
        this.numComp = (int) (Math.random() * n) + 1;
    }
}
