package Nov25;

/*
12 графов и графинь танцуют вальс. Каждый такт они перемещаются по
танцевальному залу. Нужно создать классы граф и графиня в каждом из которых будет поля
“имя” и их координаты (х и у). Каждый такт танца они будут сдвигаться в случайном
направлении на единицу. После каждого “шага” нужно записывать их новое положение в bin-
файл(удаляя при этом старое). В программе должно быть два режима. Первый это такты
танцев, который будет каждый раз спрашивать “еще такт?” до тех пор пока пользователь не
скажет остановиться. И второй режим, который считывает bin-файл и показывает на экране
координаты и имена каждого из танцующих. Изменить задачу. Вместо работы с бинарными файлами
реализовать работу с текстовыми файлами используя любой из вышеизученых классов.
*/

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Waltz {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Couple couple = new Couple("New", new Point(0, 0));
        couple.create();
        System.out.print("Dancing? Y / n: ");
        char c = scanner.next().charAt(0);
        boolean flag = true;
        while (flag) {
            if (c == 'Y') {
                couple.dance();
                System.out.println("----------------- BIN -----------------");
                couple.write();
                couple.print();
                System.out.println("----------------- TXT -----------------");
                couple.writeTxt();
                couple.printTxt();
            } else if (c == 'n') {
                flag = false;
            } else {
                System.out.println("Try again...");
            }
            System.out.print("Dancing? Y / n: ");
            c = scanner.next().charAt(0);
        }
    }
}

abstract class Human {
    private String name;
    private Point point;

    public Human(String name, Point point) {
        this.name = name;
        this.point = point;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}

class Count extends Human {
    public Count(String name, Point point) {
        super(name, point);
    }
}

class Countess extends Human {
    public Countess(String name, Point point) {
        super(name, point);
    }
}

class Couple extends Human {
    private Count count;
    private Countess countess;

    private static ArrayList<Couple> couples = new ArrayList<>(12);
    private int[][] floor = new int[150][150];   //square of floor
    private Random rnd = new Random();     //number variations of moving

    public Couple(String name, Point point, Count count, Countess countess) {
        super(name, point);
        this.count = count;
        this.countess = countess;
    }

    public Couple(String name, Point point) {
        super(name, point);
    }


    void create() {
        for (int i = 1; i <= 12; i++) {
            couples.add(new Couple("Couple" + i, new Point(i, i),
                    new Count("Count" + i, new Point(i, i)), new Countess("Countess" + i, new Point(i, i))));
        }
    }

    public void dance() {
        couples.forEach(this::step);
    }

    private void step(Couple couple) {
        boolean flag = true;
        while (flag) {
            switch (rnd.nextInt(3)) {   //variations of moving
                case 0:
                    couple.setPoint(new Point(couple.getPoint().x, couple.getPoint().y - 1));   //Left
                    break;
                case 1:
                    couple.setPoint(new Point(couple.getPoint().x, couple.getPoint().y + 1));   //Right
                    break;
                case 2:
                    couple.setPoint(new Point(couple.getPoint().x - 1, couple.getPoint().y));   //Up
                    break;
                case 3:
                    couple.setPoint(new Point(couple.getPoint().x - 1, couple.getPoint().y));   //Down
                    break;
            }
            if (couple.getPoint().x <= floor.length || couple.getPoint().y <= floor.length) {
                flag = false;
            }

        }
    }

    void write() {
        try (DataOutputStream out = new DataOutputStream(new FileOutputStream("src\\Nov25\\Couples.bin"))) {
            couples.forEach(couple -> {
                try {
                    out.writeUTF(couple.count.getName());
                    out.writeUTF(couple.countess.getName());
                    out.writeInt(couple.getPoint().x);
                    out.writeInt(couple.getPoint().y);
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            });
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    void writeTxt(){
        try (BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream("src\\Nov25\\List"))) {
            for (Couple couple : couples) {
                    writer.write(couple.toString().getBytes());
                    writer.write("\n".getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void print() {
        try (DataInputStream in = new DataInputStream(new FileInputStream("src\\Nov25\\Couples.bin"))) {
            while (true) {
                String countName = in.readUTF();
                String countessName = in.readUTF();
                int x = in.readInt();
                int y = in.readInt();
                System.out.printf("%8s  %8s \t %5s \t %3s \n", countName, countessName, x, y);
            }
        } catch (EOFException ignored) {
        } catch (IOException e) {
            System.out.println(e.getMessage());

        }
        System.out.println();
    }

    void printTxt() throws IOException {
        BufferedInputStream reader = new BufferedInputStream(new FileInputStream("src\\Nov25\\List"));
        StringBuilder tmp = new StringBuilder();
        int c;
        while ((c = reader.read()) != -1) {
            tmp.append((char) c);
        }
        System.out.println(tmp);
            //System.out.println(reader.read(toString(tmp.toString().split("\r\n"))));
    }

    @Override
    public String toString() {
        return String.format("%8s  %8s \t %5s \t %3s", count.getName(), countess.getName(), getPoint().x, getPoint().y);
    }
}