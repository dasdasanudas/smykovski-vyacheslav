package Oct8;

/*
Написать класс калькулятора, хранящего вещественное число x и понимающего следующие команды:
прибавить
к этому числу значение параметра, вычесть из него, домножить его и разделить, а также извлечь из этого числа
квадратный корень и взять тригонометрическую функцию. Написать еще один класс, кроме перечисленного
имеющий
одну ячейку памяти и понимающий команды записать в память, извлечь из памяти, добавить x к содержимому
памяти.
*/

public class Task1 {
    public static void main(String[] args) {
        MyCalculate calculate = new MyCalculate();
        calculate.setX(17);
        System.out.println("Значение x: " + calculate.getX());
        System.out.println("Значение двух x: " + calculate.plusX());
        System.out.println("Сумма: " + calculate.sum(12.4));
        System.out.println("Вычитание: " + calculate.subtraction(12.4));
        System.out.println("Произведение: " + calculate.multiplication(12.4));
        System.out.println("Деление: " + calculate.division(12.4));
        System.out.println("Квадратный корень: " + calculate.squareRoot());
        System.out.println("Синус: " + calculate.sin());
    }
}

class Calculator {
    double x;

    public double sum(double y) {
        return x + y;
    }

    public double subtraction(double y) {
        return x - y;
    }

    public double multiplication(double y) {
        return x * y;
    }

    public double division(double y) {
        return x / y;
    }

    public double squareRoot() {
        return Math.sqrt(x);
    }

    public double sin() {
        return Math.sin(x);
    }

}

class MyCalculate extends Calculator {

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double plusX() {
        return x += x;
    }

}
