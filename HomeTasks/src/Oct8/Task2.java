package Oct8;

/*Разработать три класса, которые следует связать между
собой, используя наследование:
класс Product, который имеет три элемент-данных — имя,
цена и вес товара (базовый класс для всех классов);
класс Buy, содержащий данные о количестве покупаемого
товара в штуках, о цене за весь купленный товар и о весе
товара (производный класс для класса Product и базовый
класс для класса Check);
класс Check, не содержащий никаких элемент-данных.
Данный класс должен выводить на экран информацию о
товаре и о покупке ( производный класс для класса Buy);
Для взаимодействия с данными классов разработать set- и
get—методы. Все элемент-данные классов объявлять как
private.*/

public class Task2 {
    public static void main(String[] args) {
        Check check = new Check();
        check.setName("Творог");
        check.setPrice(70.5);
        check.setWeight(0.450);
        check.setQty(2);
        check.getFullWeight();
        check.getFullPrice();
        System.out.println(check.print());
    }
}

class Product {
    private String name;
    private double price;
    private double weight;

    public Product(String name, double price, double weight) {
        this.name = name;
        this.price = price;
        this.weight = weight;
    }

    public Product() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}

class Buy extends Product {
    private int qty;
    private double fullPrice;
    private double fullWeight;

    public Buy(String name, double price, double weight, int qty, double fullPrice, double fullWeight) {
        super(name, price, weight);
        this.qty = qty;
        this.fullPrice = fullPrice;
        this.fullWeight = fullWeight;
    }

    public Buy() {
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getFullPrice() {
        return super.getPrice() * getFullWeight();
    }

    public double getFullWeight() {
        return super.getWeight() * (double) qty;
    }

}

class Check extends Buy {
    String print() {
        return "Наименование товара: " + this.getName() + "\nСтоимость: " + this.getPrice() + "\nВес: " +
                this.getWeight() + "\n\nИтого:\nКол-во: " + this.getQty() + "\nСтоимость: " + this.getFullPrice() +
                "\nВес: " + this.getFullWeight();
    }
}
