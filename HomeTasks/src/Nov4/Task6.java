package Nov4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

/*Задача 6.(*) Совершенным числом называется число, равное сумме своих делителей, меньших его
        самого. Например, 28=1+2+4+7+14. Определите, является ли данное натуральное число
        совершенным. Найдите все совершенные числа на данном отрезке (возможно, стоит применить идею
        решета Эратосфена).*/

public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        EratosthenesSieve sieve = new EratosthenesSieve();
        System.out.print("Set the limit of numbers: ");
        sieve.findPerfectNumbers(sc.nextInt());
    }
}

class EratosthenesSieve {
    private int max;
    private ArrayList<Integer> simpleNumbers = new ArrayList<>();

    private void create() {
        for (int i = 1; i < max; i++) {
            simpleNumbers.add(i);
        }

        for (int i = 2; i * i <= max; i++) {
            for (int j = i * i; j < max; j += i) {
                if (j % i == 0) {
                    simpleNumbers.set(j - 1, 0);
                }
            }
        }
        simpleNumbers.sort(Comparator.comparing(Integer::intValue));
        Integer n = 0;
        while (simpleNumbers.remove(n)) {
        }
    }

    void findPerfectNumbers(int max) {
        this.max = max;
        create();
        //Simple Find
/*        for (int i = 1; i <= max; i++) {
            int sum = 0;
            for (int j = 1; j < i; j++) {
                if (i % j == 0) {
                    sum += j;
                }
            }
            if (i == sum) {
                System.out.println(sum);
            }
        }*/

        //Find By Formula
        for (int i = 1; i <= max; i++) {
            int sum = 0;
            if (simpleNumbers.contains((int) Math.pow(2, i) - 1)) {
                sum = (int) Math.pow(2, i - 1) * ((int) Math.pow(2, i) - 1);
                if (sum < max) {
                    System.out.println(sum);
                } else {
                    break;
                }
            }
        }
    }
}
