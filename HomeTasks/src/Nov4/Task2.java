package Nov4;

import java.util.*;

/*
Задача 2. Л. Кэрролл в своем дневнике писал, что он тщетно трудился, пытаясь найти хотя бы три
        прямоугольных треугольника равной площади, у которых длины сторон были бы выражены
        натуральными числами. Составьте программу для решения этой задачи, если известно, что такие
        треугольники существуют. Напишите программу, которая находит все прямоугольные треугольники
        (длины стороны выражаются натуральными числами), площадь которых не превышает данного
        числа S.
*/

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int s = sc.nextInt();
        ArrayList<Triangle> list = new ArrayList<>();
        Triangle triangle = new Triangle();

        for (int a = 1; a < s; a++) {
            for (int b = a + 1; b < s; b++) {
                double c = Math.pow(Math.pow(a, 2) + Math.pow(b, 2), 1.0 / 2);
                if (c - (int) c == 0) {
                    if (a + b > c | b + c > a | a + c > b) {
                            if (triangle.square(a, b, (int) c) <= s)
                                list.add(triangle.create(a, b, (int) c));
                    }
                }
            }
        }
        triangle.print(list);
    }
}

class Triangle {
    int a;
    int b;
    int c;
    int square = a * b / 2;

    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.square = a * b / 2;
    }

    public Triangle() {
    }

    int perimeter(int a, int b, int c) {
        return a + b + c;
    }

    double square(int a, int b, int c) {
        return Math.sqrt(perimeter(a, b, c) / 2 * (perimeter(a, b, c) / 2 - a) * (perimeter(a, b, c) / 2 - b) * (perimeter(a, b, c) / 2 - c));
    }

    Triangle create(int a, int b, int c) {
        double cornerA = Math.asin(2 * square(a, b, c) / (a * b)) * 180 / Math.PI;
        double cornerB = Math.asin(2 * square(a, b, c) / (a * c)) * 180 / Math.PI;
        double cornerC = Math.asin(2 * square(a, b, c) / (b * c)) * 180 / Math.PI;

        if (cornerA == 90 || cornerB == 90 || cornerC == 90) {
            return new Triangle(a, b, c);
        }
        return null;
    }

    void sort(ArrayList<Triangle> list) {
        list.sort(Comparator.comparing(Triangle::getSquare));
    }

    void print(ArrayList<Triangle> list) {
        sort(list);
        boolean flag = true;
        for (int i = 0; i < list.size() - 1; ) {
            if (list.get(i).getSquare() == list.get(i + 1).getSquare() && list.get(i).a != list.get(i + 1).a) {
                System.out.println(list.get(i));
                System.out.println(list.get(i + 1));
                i++;
                flag = true;
                while (list.get(i).getSquare() == list.get(i + 1).getSquare() && list.get(i).a != list.get(i + 1).a) {
                    System.out.println(list.get(i + 1));
                    i++;
                }
            } else {
                i++;
                if (flag) {
                    System.out.println();
                    flag = false;
                }
            }
        }
    }

    @Override
    public String toString() {
        return a + " " + b + " " + c;
    }

    public int getSquare() {
        return square;
    }
}

