package Nov4;

import java.util.Scanner;

/*
Задача 5. Б. Кордемский указывает одно интересное число 145, которое равно сумме факториалов
        своих цифр: 145=1!+4!+5!. Он пишет, что неизвестно, есть ли еще такие числа,
        удовлетворяющие названному условию. Написать программу по нахождению таких чисел.
*/

public class Task5 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Set the limit of number: ");
        int num = sc.nextInt();
        Task5 obj = new Task5();

        for (int i = 1; i < num; i++) {
            if (i == obj.sum(i)) {
                System.out.println(i);
            }
        }
    }

    int sum(int x){
        int factorialSum = 0;
        while (x > 0) {
                factorialSum +=factorial(x % 10);
                x /= 10;
            }
        return factorialSum;
        }

    int factorial(int x) {
        if (x <= 1) {
            return 1;
        }
        return x * factorial(x - 1);
    }
}
