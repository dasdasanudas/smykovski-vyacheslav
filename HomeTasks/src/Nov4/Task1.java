package Nov4;

import java.util.ArrayList;
import java.util.Scanner;

/*
Задача 1. Пифагоровы числа. Три натуральных числа a, b и c образуют пифагорову тройку, если
        c2=a2+b2. Пифагорова тройка называется основной, если наибольший общий делитель ее чисел
        равен единице. Например, 3, 4, 5 - основная тройка, 6, 8, 10 - производная тройка. Найдите все
        основные пифагоровы тройки, числа в которых не превышают данное число max. Напишите
        программу нахождения всех решений на отрезке [2;100] уравнения x2+y2=zn, n - известное
        натуральное число.
*/

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Set interval number: ");
        int a = sc.nextInt();
        System.out.print("Set next interval number: ");
        int b = sc.nextInt();
        System.out.print("Set power n for formula x^2 + y^2 = z^n: ");
        int n = sc.nextInt();
        Nod nod = new Nod();
        nod.findPythagoreanTriple(a, b, n);
        nod.print();
    }

}

class Nod {
    static ArrayList<Nod> list = new ArrayList<>();
    int x;
    int y;
    int z;

    public Nod(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Nod() {
    }

    @Override
    public String toString() {
        return x + " " + y + " " + z;
    }

    void findPythagoreanTriple(int a, int b, int n) {
        for (int i = a; i <= b; i++) {
            for (int j = i; j <= b; j++) {
                double c = Math.pow(Math.pow(i, 2) + Math.pow(j, 2), 1.0 / n);
                if (c - (int) c == 0) {
                        if (c < Math.max(a, b)) {
                            nod(i, j, (int) c);
                        }
                }
            }
        }
    }

    void nod(int x, int y, int z) {
        int noda = 1;
        for (int i = 2; i <= z; i++) {
            if (x % i == 0 && y % i == 0 && z % i == 0) {
                noda = i;
            }
        }
        if (noda == 1) {
            list.add(new Nod(x, y, z));
        }
    }

    void print() {
        for (Nod element : list) {
            System.out.println(element.toString());
        }
    }
}
