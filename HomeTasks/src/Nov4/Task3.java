package Nov4;

/*
Задача 3. Напишите программу, которая определяет, сколько можно купить быков, коров и телят,
        платя за быка 10 рублей, за корову - 5 рублей, а за теленка - 50 копеек, если на 100 рублей надо
        купить 100 голов скота?
*/

public class Task3 {
    public static void main(String[] args) {
        double allMoney = 100;
        double costOfCow = 5;
        double costOfBull = 10;
        double costOfCalf = 0.5;
        int count = 100;

        int bull;
        int cow;
        int calf;

        for (bull = 0; bull <= allMoney / costOfBull ; bull++) {
            for (cow = 0; cow <= (allMoney - bull * costOfBull) / costOfCow; cow++) {
                for (calf = 0; calf <= (allMoney - bull * costOfBull - cow * costOfCow) / costOfCalf; calf++) {
                    if (count - (bull + cow + calf) == 0 && allMoney - bull * costOfBull - cow * costOfCow - costOfCalf * calf == 0) {
                        System.out.printf("Count of bulls: %s, of cows: %s, of calves: %s", bull, cow, calf);
                    }
                }
            }
        }
    }
}

