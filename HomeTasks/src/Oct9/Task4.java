package Oct9;

import java.util.Scanner;

/*
Написать игру “камень, ножницы, бумага”. Создать интерфейс класс Player с методами:
        ● getName возвращает имя игрока
        ● getMove возвращает решение игрока на текущий ход
        ● addPoint добавляет одно очко игроку, когда он выигрывает
        ● getPoints возвращает количество очков игрока
        Создать двух игроков-наследников Human и Computer
        Имя человеческого игрока Human должно запрашиваться с клавиатуры
        Имя компьютерного игрока можно вернуть любое
        Решение игрока должно запрашиваться с клавиатуры
        Решение компьютера должно выбираться случайно (см. Math.random)
        Создать класс Game, принимающий в конструкторе двух игроков имеющий метод play
        Метод play описывает процесс игры:
        1. Запросить имена игроков
        2. Получить решения игроков и сравнить
        3. Победившему игроку добавляется одно очко
        4. Напечатать результат игры - количество очков у каждого игрока
        5. Спросить у пользователя, хочет ли он продолжить игру
        6. Если да - Перейти к шагу 2. Если нет, к шагу 7
        7. Выйти
*/

public class Task4 {
    public static void main(String[] args) {
        Player comp = new Computer();
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите своё имя: ");
        String name = sc.nextLine();
        Player human = new Human(name);
        Game game = new Game(comp, human);
        game.play();
    }
}

class Game {
    Game(Player human, Player comp) {
        this.human = human;
        this.comp = comp;
    }

    Player comp;
    Player human;

    public void play() {
        System.out.println("Камень: 1 \t Ножницы: 2 \t Бумага: 3");
        boolean repeat = true;
        while (repeat) {
            int move = human.getMove();
            int compMove = comp.getMove();
            if (move == compMove)
                System.out.println("Дружеская ничья");
            if (move == 1 && compMove == 2) {
                human.addPoint();
                System.out.println("Победил(а) " + human.getName());
            }
            if (move == 2 && compMove == 1) {
                comp.addPoint();
                System.out.println("Победил(а) " + comp.getName());
            }
            if (move == 2 && compMove == 3) {
                human.addPoint();
                System.out.println("Победил(а) " + human.getName());
            }
            if (move == 3 && compMove == 2) {
                comp.addPoint();
                System.out.println("Победил(а) " + comp.getName());
            }
            if (move == 3 && compMove == 1) {
                human.addPoint();
                System.out.println("Победил(а) " + human.getName());
            }
            if (move == 1 && compMove == 3) {
                comp.addPoint();
                System.out.println("Победил(а) " + comp.getName());
            }
            System.out.println("Кол-во очков у " + human.getName() + ": " + human.getPoints());
            System.out.println("Кол-во очков у " + comp.getName() + ": " + comp.getPoints());
            Scanner sc = new Scanner(System.in);
            System.out.print("Продолжить игру? true / false: ");
            repeat = sc.nextBoolean();
        }
    }


}

interface Player {
    String getName();

    int getMove();

    int addPoint();

    int getPoints();
}

class Human implements Player {
    String name;
    int move;
    int points;

    public Human() {
    }

    public Human(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getMove() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите действие (1, 2 либо 3): ");
        int move = sc.nextInt();
        return move;
    }

    public void setMove(int move) {
        this.move = move;
    }

    @Override
    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int addPoint() {
        return points++;
    }
}

class Computer implements Player {
    private String name = "Owner";
    int move;
    int points;

    @Override
    public String getName() {
        return name;
    }

    public int getMove() {
        return (int) Math.random() * 3 + 1;
    }

    public int getPoints() {
        return points;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMove(int move) {
        this.move = move;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int addPoint() {
        return points++;
    }
}