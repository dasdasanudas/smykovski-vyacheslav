package Oct9;

/*
Создать абстрактный класс “Orc” с переменными
для показателей урона, брони, здоровья и типа атаки,
содержащий также абстрактный метод Attack.
Наследовать от него 3 обычных класса “OrcPeon”,
“OrcGrunt” и “OrcShaman”, для каждого из которых
установить тип атаки ”crushing”, “hacking” и “magical”(как
переменная класса типа String), также определить для
каждого из классов переменные из абстрактного класса и
функцию Attack, чтобы она выводила на экран показатель
атаки и ее тип.
*/

public class Task3 {
    public static void main(String[] args) {
        OrcPeon peon = new OrcPeon(1, 10, 100);
        OrcGrunt grunt = new OrcGrunt(200, 1000, 1000);
        OrcShaman shaman = new OrcShaman(300, 100, 500);
        peon.attack();
        grunt.attack();
        shaman.attack();
    }
}

abstract class Orc {
    int damage;
    int armor;
    int hp;
    String attackType;

    public Orc(int damage, int armor, int hp) {
        this.damage = damage;
        this.armor = armor;
        this.hp = hp;
    }

    abstract void attack();
}

class OrcPeon extends Orc {

    public OrcPeon(int damage, int armor, int hp) {
        super(damage, armor, hp);
        attackType = "crushing";
    }

    public void attack() {
        System.out.println("Показатель атаки: " + damage + "\t Тип атаки: " + attackType);
    }
}

class OrcGrunt extends Orc {

    public OrcGrunt(int damage, int armor, int hp) {
        super(damage, armor, hp);
        attackType = "hacking";
    }

    public void attack() {
        System.out.println("Показатель атаки: " + damage + "\t Тип атаки: " + attackType);
    }
}

class OrcShaman extends Orc {

    public OrcShaman(int damage, int armor, int hp) {
        super(damage, armor, hp);
        attackType = "magical";
    }

    public void attack() {
        System.out.println("Показатель атаки: " + damage + "\t Тип атаки: " + attackType);
    }
}
