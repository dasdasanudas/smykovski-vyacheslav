package Oct9;

/*
Создать абстрактный класс “Rodent” с переменными для пола, веса, цвета шкуры
и скорости бега, содержащий также абстрактные методы Run, Jump и Eat. Наследовать
от него 3 обычных класса “Hamster”, “Chinchilla” и “Rat”, для каждого из которых
переопределить метод Run, чтобы он выводил название класса и скорость бега грызуна
на экран.
*/

public class Task2 {
    public static void main(String[] args) {
        Hamster hamster = new Hamster('m', 150, "brown", 35);
        Chinchilla chinchilla = new Chinchilla('f', 180, "black", 27);
        Rat rat = new Rat('m', 100, "white", 62);
        hamster.run();
        chinchilla.run();
        rat.run();
    }
}

abstract class Rodent {
    char sex;
    double weight;
    String skinColor;
    double speedRun;

    public Rodent(char sex, double weight, String skinColor, double speedRun) {
        this.sex = sex;
        this.weight = weight;
        this.skinColor = skinColor;
        this.speedRun = speedRun;
    }

    abstract void run();

    abstract void jump();

    abstract void eat();
}

class Hamster extends Rodent {
    public Hamster(char sex, double weight, String skinColor, double speedRun) {
        super(sex, weight, skinColor, speedRun);
    }

    @Override
    public void run() {
        System.out.println("Класс: Hamster \t Скорость бега: " + speedRun + " м/мин");
    }

    public void jump() {
    }

    public void eat() {
    }
}

class Chinchilla extends Rodent {

    public Chinchilla(char sex, double weight, String skinColor, double speedRun) {
        super(sex, weight, skinColor, speedRun);
    }

    @Override
    public void run() {
        System.out.println("Класс: Chinchilla \t Скорость бега: " + speedRun + " м/мин");
    }

    public void jump() {
    }

    public void eat() {
    }
}

class Rat extends Rodent {

    public Rat(char sex, double weight, String skinColor, double speedRun) {
        super(sex, weight, skinColor, speedRun);
    }

    @Override
    public void run() {
        System.out.println("Класс: Rat \t Скорость бега: " + speedRun + " м/мин");
    }

    public void jump() {
    }

    public void eat() {
    }
}
