package Oct9;

/*
Создать абстрактный класс “Dog” с переменными для клички, пола, веса собак,
содержащий также абстрактные методы Voice и Action. Наследовать от него три
обычных класса “DomesticDog”, “ServiceDog” и “HomelessDog”, для каждого из которых
определить строковую переменную address, organization, area соответственно.
*/

public class Task1 {
    public static void main(String[] args) {
        DomesticDog dog1 = new DomesticDog("Sharik", 'm', 12.7, "Ayanot", "Tlalim", "7th building");
        ServiceDog dog2 = new ServiceDog("Lyusya", 'f', 18.3, "Ayanot", "Tlalim", "7th building");
        HomelessDog dog3 = new HomelessDog("Bobik", 'm', 9.2, "Ayanot", "Tlalim", "7th building");
        System.out.println("Кличка: " + dog1.aka + " Пол: " + dog1.sex + " Вес: " + dog1.weight + " Адрес: " + dog1.address + " Организация: " + dog1.organization + " Место: " + dog1.area);
        System.out.println("Кличка: " + dog2.aka + " Пол: " + dog2.sex + " Вес: " + dog2.weight + " Адрес: " + dog2.address + " Организация: " + dog2.organization + " Место: " + dog2.area);
        System.out.println("Кличка: " + dog3.aka + " Пол: " + dog3.sex + " Вес: " + dog3.weight + " Адрес: " + dog3.address + " Организация: " + dog3.organization + " Место: " + dog3.area);
    }
}

abstract class Dog {
    String aka;
    char sex;
    double weight;

    public Dog(String aka, char sex, double weight) {
        this.aka = aka;
        this.sex = sex;
        this.weight = weight;
    }

    abstract void voice();

    abstract void action();
}

class DomesticDog extends Dog {
    String address;
    String organization;
    String area;

    public DomesticDog(String aka, char sex, double weight, String address, String organization, String area) {
        super(aka, sex, weight);
        this.address = address;
        this.organization = organization;
        this.area = area;
    }

    public void voice() {
    }

    public void action() {
    }
}

class ServiceDog extends Dog {
    String address;
    String organization;
    String area;

    public ServiceDog(String aka, char sex, double weight, String address, String organization, String area) {
        super(aka, sex, weight);
        this.address = address;
        this.organization = organization;
        this.area = area;
    }

    public void voice() {
    }

    public void action() {
    }
}

class HomelessDog extends Dog {
    String address;
    String organization;
    String area;

    public HomelessDog(String aka, char sex, double weight, String address, String organization, String area) {
        super(aka, sex, weight);
        this.address = address;
        this.organization = organization;
        this.area = area;
    }

    public void voice() {
    }

    public void action() {
    }
}
