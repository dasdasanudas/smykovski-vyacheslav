package Nov26;

/*
Создать класс студент с полями фамилия, имя, массив с оценками и поле со средним
        значением оценки. Используя сериализацию записать в файл данные о 10 студентах и
        впоследствии считать из файла и вывести информацию о них. Модифицировать прошлую задачу,
        исключив поле средней оценки из сериализованных данных.
*/

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class Serialization {
    public static void main(String[] args) {
        int studentsCount = 10;
        ArrayList<Student> students = new ArrayList<>(studentsCount);
        for (int i = 0; i < studentsCount; i++) {
            students.add(new Student("LastName" + i, "Name" + i, new int[]{2, 4, 3, 5}));
        }
        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("C:\\Users\\dasbr\\IdeaProjects\\HomeTasks\\src\\Nov26\\Students_List"))){
            output.writeObject(students);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}

class Student implements Serializable {
    private String lastName;
    private String name;
    private int[] grades;
    private transient int averageGrade;

    public Student(String lastName, String name, int[] grades) {
        this.lastName = lastName;
        this.name = name;
        this.grades = grades;
        averageGrade = Arrays.stream(grades).sum() / grades.length;
    }
}
