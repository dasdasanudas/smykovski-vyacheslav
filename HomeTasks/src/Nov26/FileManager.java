package Nov26;

/*
Создать программу для работы с каталогами. На вход указать путь из которого начать. В
программе должны быть функции показать список файлов и папок в текущей директории,
создать/удалить каталог, вывести файлы определенного расширения.
*/

import java.io.File;
import java.io.FileFilter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class FileManager {
    private static String cmd;
    private static File dir;
    private static String path;

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            while (true) {
                cmd = sc.nextLine();
                if (cmd.equals("ls")) {
                    ls();
                } else if (cmd.equals("help")) {
                    help();
                } else if (cmd.substring(0, 3).equals("cd ")) {
                    dir = new File(cmd.substring(3));
                    if (!dir.exists() || !dir.isDirectory())
                        System.out.println("Directory not found!");
                } else if (cmd.substring(0, 6).equals("mkdir ")) {
                    path = cmd.substring(6);
                    boolean created = new File(dir.getAbsolutePath() + path).mkdir();
                    if (!created)
                        System.out.println("Directory " + path + " not created!");
                }else if (cmd.substring(0, 4).equals("del ")) {
                    path = cmd.substring(4);
                    boolean created = new File(dir.getAbsolutePath() + path).delete();
                    if (!created)
                        System.out.println("File or directory " + path + " not deleted!");
                } else if (cmd.substring(0, 4).equals("dir ")) {
                    path = cmd.substring(4);
                    File[] sorted = dir.listFiles(new Filter());
                    assert sorted != null;
                    for (File file : sorted) {
                        System.out.println(file);
                    }
                }
            }
        }catch (NullPointerException e){
            System.out.println(e.getMessage());
        }
    }

    private static void ls() {
        File[] root = dir.listFiles();
        assert root != null;
        for (File file : root) {
            System.out.println(file);
        }
    }

    private static void help() {
        HashMap<String, String> commands = new HashMap<>();
        commands.put("cd PATH", "Change directory with PATH\n");
        commands.put("mkdir NAME", "Make the directory with NAME\n");
        commands.put("del NAME", "Delete file or directory with NAME\n");
        commands.put("dir FILTER", "Show content of the directory throw FILTER (.txt or .pdf ... etc.)\n");
        commands.put("ls", "\t\t Show content of the directory\n");

        for (Map.Entry<String, String> command : commands.entrySet()) {
            System.out.printf("%s \t\t %s", command.getKey(), command.getValue());
        }
    }

    static class Filter implements FileFilter {
        @Override
        public boolean accept(File pathname) {
            if (!pathname.isFile())
                return false;
            if (pathname.getAbsolutePath().endsWith(path))
                return true;
            return false;
        }
    }
}
