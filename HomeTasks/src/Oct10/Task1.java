package Oct10;

import java.util.Scanner;

/*
Создать два enum - класса. В одном перечислены все дни недели, в другом все месяца.
Создать класс DateOfMeet в котором будут поля этих enum - классов, поле с названием встречи,
поле с описание встречи и два поля с часами и минутами.
Добавить в него метод show который показывает информацию о встрече.
Создать массив из пяти таких встреч и заполнить их с клавиатуры в цикле.
*/

public class Task1 {
    public static void main(String[] args) {

        Meeting[] meetings = new Meeting[2];
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 2; i++) {
            meetings[i] = new Meeting();
            System.out.println("Мероприятие №: " + (i + 1));
            System.out.print("Укажите дату: ");
            meetings[i].setDate(sc.nextInt());
            System.out.print("Укажите месяц: ");
            meetings[i].setMonth(Month.values()[sc.nextInt() - 1]);
            System.out.print("Укажите день: ");
            meetings[i].setDay(Day.values()[sc.nextInt() - 1]);
            System.out.print("Укажите название встречи: ");
            meetings[i].setMeeting(sc.next());
            System.out.print("Укажите описание: ");
            meetings[i].setAbout(sc.next());
            System.out.print("Укажите время начала (часы): ");
            meetings[i].setHour(sc.nextInt());
            System.out.print("Укажите время начала (минуты): ");
            meetings[i].setMin(sc.nextInt());
        }

        for (Meeting meeting : meetings) {
            System.out.println(meeting.show());
            System.out.println();
        }

    }
}

class Meeting {
    private int date;
    private Month month;
    private Day day;
    private String meeting;
    private String about;
    private int hour;
    private int min;

    public Meeting(int date, Month month, Day day, String meeting, String about, int hour, int min) {
        this.date = date;
        this.month = month;
        this.day = day;
        this.meeting = meeting;
        this.about = about;
        this.hour = hour;
        this.min = min;
    }

    public Meeting() {
    }

    public String show() {
        return "Встреча: " + getMeeting() + "\t Описание: " + getAbout() +
                "\nНачало: " + getDate() + " " + getMonth() + " " + getDay() + "\t" +
                String.format("%d:%02d", getHour(), getMin());
    }


    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
/*        Scanner sc = new Scanner(System.in);
        System.out.print("Укажите месяц: ");
        this.month = sc.nextLine(Month)[sc.nextInt()];*/
    }

    public String getMeeting() {
        return meeting;
    }

    public void setMeeting(String meeting) {
        this.meeting = meeting;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }
}

enum Day {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}

enum Month {
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER;
}
