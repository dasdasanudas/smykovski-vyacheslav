package Oct10;

import java.util.Scanner;

/*
На основе первой задачи. Создать класс BookOfMeeting. В нём создать массив из 365 дней.
Заполнить каждый день значением дня недели и месяца и задать часы и минуты встречи равные -1.
Это означает, что в этот день встречи нет.
-Добавить в класс функцию добавить встречу в календарь. Она должна проверять введенные даты
и если в этот день нет встреч, то устанавливать заданное время начала встречи.
-Добавить в класс функцию показать все имеющиеся встречи, которая выводит на экран все дни со
временем встреч. Дни в которые встреч нет печатать не нужно.
*/

public class BookOfMeeting {
    public static void main(String[] args) {
        Meeting[] days = new Meeting[365];
        int[] daysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int k = 0;
        int z = 0;
        for (int i = 0; i < daysInMonth.length; i++) {
            for (int j = 0; j < daysInMonth[i]; j++) {
                days[z] = new Meeting();
                days[z].setDate(j + 1);
                days[z].setMonth(Month.values()[i]);

                days[z].setHour(-1);
                days[z].setMin(-1);
                if (k < 7) {
                    days[z].setDay(Day.values()[k]);
                    k++;
                }
                if (k == 7) {
                    k = 0;
                }
                z++;
            }
        }
        addMeeting(days);
        displayMeetings(days);
    }

    public static void addMeeting(Meeting[] days) {
        int date;
        int month;
        Scanner sc = new Scanner(System.in);
        System.out.println("Добавить встречу");
        System.out.print("Укажите число: ");
        date = sc.nextInt();
        date--;
        System.out.print("Укажите месяц: ");
        month = sc.nextInt();

        if (month == 2)
            date += 31;
        if (month > 2 && month < 8)
            date = (month - 1) * 31 - (month - 1) / 2 - 2 + date;
        if (month > 7 && month < 13)
            if (month % 2 == 0)
                date = (month - 1) * 31 - (month - 1) / 2 - 2 + date;
            else
                date = (month - 1) * 31 - (month - 1) / 2 - 1 + date;
        if (days[date].getHour() == -1 && days[date].getMin() == -1) {
            System.out.print("Укажите название встречи: ");
            days[date].setMeeting(sc.next());
            System.out.print("Укажите описание: ");
            days[date].setAbout(sc.next());
            System.out.print("Укажите время начала (часы): ");
            days[date].setHour(sc.nextInt());
            System.out.print("Укажите время начала (минуты): ");
            days[date].setMin(sc.nextInt());
        } else {
            System.out.println("В этот день у Вас уже запланирована встреча: \n" + (days[date].show()));
            System.out.println();
        }
    }

    public static void displayMeetings(Meeting[] days) {
        for (Meeting meeting : days) {
            if (meeting.getHour() != -1 && meeting.getMin() != -1) {
                System.out.println(meeting.show());
                System.out.println();
            }
        }

    }
}
