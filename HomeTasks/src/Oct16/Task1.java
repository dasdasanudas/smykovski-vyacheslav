package Oct16;

/*
Создать функцию, в которую передается либо целочисленное, либо
вещественное, либо строковое значение, после чего на экран выводится тип
переданных в функцию данных и само значение.
*/

public class Task1 {
    public static void main(String[] args) {
        Integer x = 15;
        Double y = 10.01;
        String z = "qwerty";
        System.out.println(print(x));
        System.out.println(print(y));
        System.out.println(print(z));
    }
    public static  <T> String print(T item){
            return item.getClass().getSimpleName() + "\t"  + item.toString();
    }
}
