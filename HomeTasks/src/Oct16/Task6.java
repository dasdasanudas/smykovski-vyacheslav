package Oct16;

/*
Создать класс “Elf” с полями для
        показателей здоровья и урона и функцией “Strike” типа void, от которого наследовать
        3 класса “ElfWarrior”, “ElfArcher”, “ElfMage”, в каждом из которых переопределить
        функцию “Strike”, также для каждого из классов переопределить функцию toString,
        через которую будет выводиться информация о классе. Написать класс-
        обобщение, который принимает на вход только класс “Elf” и его потомков.
*/

public class Task6 {
    public static void main(String[] args) {
        ElfWarrior warrior = new ElfWarrior(200, 100);
        Person.function(warrior);
    }
}

abstract class Elf {
    int hp;
    int attack;

    public Elf(int hp, int attack) {
        this.hp = hp;
        this.attack = attack;
    }

    abstract void Strike();
}

class ElfWarrior extends Elf {

    public ElfWarrior(int hp, int attack) {
        super(hp, attack);
    }

    void Strike() {
        System.out.println("Damage: " + attack + "\t HP: " + hp);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}

class ElfArcher extends Elf {
    public ElfArcher(int hp, int attack) {
        super(hp, attack);
    }

    void Strike() {
        System.out.println("Damage: " + attack + "\t HP: " + hp);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}

class ElfMage extends Elf {
    public ElfMage(int hp, int attack) {
        super(hp, attack);
    }

    void Strike() {
        System.out.println("Damage: " + attack + "\t HP: " + hp);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}

class Person<T extends Elf> {
    public static <T extends Elf> void function(T t){
        System.out.println(t.toString());
        t.Strike();
    }
}