package Oct16;

/*
Создать функцию, в которую передается либо символьное, либо строковое
значение, после чего вычисляется и возвращается код символа, либо сумма кодов
символов.
*/

public class Task4 {
    public static void main(String[] args) {
        Character ch = 'm';
        String st = "wtf";
        System.out.println(print(ch));
        System.out.println(print(st));
    }

    public static <T> int print(T item) {
        int sum = 0;
        if (item instanceof Character) {
            sum = (int) (Character) item;
        }

        if (item instanceof String) {
            char[] arrStr = ((String) item).toCharArray();
            for (char x : arrStr
            ) {
                sum += (int) x;
            }
        }
        return sum;
    }

}
