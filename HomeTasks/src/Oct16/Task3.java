package Oct16;

/*
Создать функцию, в которую передаются два значения, каждое из которых
может являться либо целочисленным, либо вещественным, после чего
определяется тип, и функция возвращает их сумму либо в виде целочисленного,
либо в виде вещественного значения.
*/

public class Task3 {
    public static void main(String[] args) {
        Integer a = 1;
        Integer b = 2;
        Double c = 1.7;
        Double d = 1.3;
        System.out.println(sum(a, b));
        System.out.println(sum(c, d));
    }

    public static <T> T sum(T item, T item2) {
        if (item instanceof Integer) {
            Integer result = ((Integer) item).intValue() + ((Integer) item2).intValue();
            return (T) result;
        }
        if (item instanceof Double) {
            Double result = ((Double) item).doubleValue() + ((Double) item2).doubleValue();
            return (T) result;
        }
        return (T) item;
    }
}
