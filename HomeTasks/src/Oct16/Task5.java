package Oct16;

import java.util.Arrays;

/*
Создать функцию, в которую передается массив логических,
целочисленных, либо вещественных значений, после чего распознается тип
данных в массиве и уже в пределах функции создается массив с полученным
типом, в который записываются переданные в функцию значения.
*/

public class Task5 {
    public static void main(String[] args) {
        Boolean[] arrB = new Boolean[10];
        arrB[0] = true;
        arrB[1] = false;
        Integer[] arrI = new Integer[10];
        arrI[0] = 1;
        arrI[1] = 2;
        Double[] arrD = new Double[10];
        arrD[0] = 1.3;
        arrD[1] = 2.7;
        print(arrB);
        print(arrI);
        print(arrD);

    }
    public static <T> void print(T[] item) {

        if (item instanceof Boolean[]) {
            Boolean[] arr = (Boolean[]) item;
            System.out.println(Arrays.toString(arr));
        }
        if (item instanceof Integer[]) {
            Integer[] arr = (Integer[]) item;
            System.out.println(Arrays.toString(arr));
        }
        if (item instanceof Double[]) {
            Double[] arr = (Double[]) item;
            System.out.println(Arrays.toString(arr));
        }
    }
}
