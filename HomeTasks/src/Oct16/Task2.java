package Oct16;

/*
Создать функцию, в которую передается логическое, либо целочисленное
значение, после чего распознается тип переданных данных. Если было передано
логическое значение false, либо целочисленное значение 0, функция возвращает
строку “Верно”, во всех остальных случаях возвращается строка “Неверно”.
*/

public class Task2 {
    public static void main(String[] args) {
        Integer x = 0;
        Boolean y = true;
        System.out.println(print(x));
        System.out.println(print(y));
    }
    public static  <T> String print(T item){
        String result = "Неверно";
        if (item instanceof Integer) {
            if ((Integer) item == 0)
                result = "Верно";
        }
        if (item instanceof Boolean) {
            if (!(Boolean) item)
                result = "Верно";
        }
        return result;
    }
}