package Oct3;

/*
Создать класс “Human” с двумя строковыми переменными для имени и фамилии, вещественной
переменной для возраста и логической переменной для пола. Добавить в класс конструкторы с
параметрами и без них.
*/

public class Task4 {
    public static void main(String[] args) {
        Human human = new Human("Миша", "Ронжин", 27, true);
        System.out.println("Имя: " + human.name + "\t Фамилия: " + human.lastName + "\t Возраст: " + human.age + "\t Мужской пол: " + human.manSex);
    }
}
class Human {
    String name;
    String lastName;
    double age;
    boolean manSex;

    public Human(String name, String lastName, double age, boolean manSex) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.manSex = manSex;
    }

    public Human() {
        this("Ира", "Хвост", 30, false);
    }
    public Human(String name) {
        this(name, "Хвост", 30, false);
        this.name = name;
    }

    public Human(String name, String lastName) {
        this(name, lastName, 30, false);
        this.name = name;
        this.lastName = lastName;
    }

    public Human(String name, String lastName, double age) {
        this(name, lastName, age, false);
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }
}
