package Oct3;

/*
Создать класс “Polynomial” с шестью вещественными показателями для коэффициентов при каждом
члене в формуле x5+ x4+x3+x2+x+c. Добавить в класс функции для расчета значения полинома в
указанной точке x, а также значения производной, также в указанной точке x.
*/

public class Task8 {
    public static void main(String[] args) {
        Polynomial polynomial = new Polynomial(14, 28.6, 3, 7.2, 10.1, 9.5);
        System.out.println("Значение переменных a: " + polynomial.getA() + ", b: " + polynomial.getB() +
                ", c: " + polynomial.getC() + ", d: " + polynomial.getD() + ", e: " + polynomial.getE() +
                ", f: " + polynomial.getF());
        System.out.println("Значение полинома: " + polynomial.operation(4));
        System.out.println("Значение производной: " + polynomial.derivative(4));
    }
}
class Polynomial {
    private double a;
    private double b;
    private double c;
    private double d;
    private double e;
    private double f;

    public Polynomial(double a, double b, double c, double d, double e, double f) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }


    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public double getE() {
        return e;
    }

    public void setE(double e) {
        this.e = e;
    }

    public double getF() {
        return f;
    }

    public void setF(double f) {
        this.f = f;
    }

    double operation(double x) {
        return a * Math.pow(x, 5) + b * Math.pow(x, 4) + d * Math.pow(x, 3) + e * Math.pow(x, 2)+ d * x + c;
    }

    double derivative(double x) {
        return 5 * Math.pow(x, 4) + 4 * Math.pow(x, 3) + 3 * Math.pow(x, 2) + 2 * Math.pow(x, 1) + Math.pow(x, 0) + 0;
    }
}
