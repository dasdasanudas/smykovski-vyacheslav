package Oct3;

/*
Создать класс “Monster” со строковой переменной для имени, целочисленной переменной для очков
здоровья, двумя вещественными переменными для силы атаки и показателя защиты. Добавить в класс
конструктор с параметрами и функцию для вывода всех параметров, на экран.
*/

public class Task5 {
    public static void main(String[] args) {
    Monster monster = new Monster("Baium", 1000000, 1000, 2000);
    monster.print(monster.name,monster.hp, monster.attack, monster.defence);
    }
}

class Monster {
    String name;
    int hp;
    double attack;
    double defence;

    public Monster(String name, int hp, double attack, double defence) {
        this.name = name;
        this.hp = hp;
        this.attack = attack;
        this.defence = defence;
    }

    public void print(String name, int hp, double attack, double defence) {
        System.out.println("Имя: " + name + "\t Здоровье: " + hp + "\t Сила атаки: " + attack + "\t Защита: " + defence);
    }
}
