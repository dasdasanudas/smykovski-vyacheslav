package Oct3;

/*
Создать класс “Fraction” с двумя вещественными переменными для задания значений числителя и
знаменателя. Добавить в класс перегрузку функций сложения, вычитания, умножения и деления, а также
функцию для вывода дроби.
*/

public class Task2 {
    public static void main(String[] args) {

        Fraction fraction = new Fraction(1, 3);
        Fraction fraction1 = new Fraction(1, 2);
        Operation operation = new Operation();
        System.out.println("Задача №2: Действие с дробями: " + fraction.print() + " и " + fraction1.print());
        System.out.println("Сумма: " + operation.sum(fraction, fraction1).print());
        System.out.println("Разность: " + operation.difference(fraction, fraction1).print());
        System.out.println("Умножение: " + operation.multiple(fraction, fraction1).print());
        System.out.println("Деление: " + operation.division(fraction, fraction1).print());

    }
}

class Fraction {
    int numerator;
    int denominator;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public String print() {
        String res = numerator + "/" + denominator;
        return res;
    }
}

class Operation {
    Fraction sum(Fraction lhs, Fraction rhs) {
        int sum;
        int znam;
        sum = ((lhs.numerator * rhs.denominator) + (lhs.denominator * rhs.numerator));
        znam = lhs.denominator * rhs.denominator;
        return new Fraction(sum, znam);
    }

    Fraction difference(Fraction lhs, Fraction rhs) {
        int dif;
        int znam;
        dif = ((lhs.numerator * rhs.denominator) - (lhs.denominator * rhs.numerator));
        znam = lhs.denominator * rhs.denominator;
        return new Fraction(dif, znam);
    }

    Fraction multiple(Fraction lhs, Fraction rhs) {
        int dif;
        int znam;
        dif = ((lhs.numerator * rhs.denominator) * (lhs.denominator * rhs.numerator));
        znam = lhs.denominator * rhs.denominator;
        return new Fraction(dif, znam);
    }

    Fraction division(Fraction lhs, Fraction rhs) {
        int dif;
        int znam;
        dif = ((lhs.numerator * rhs.denominator) / (lhs.denominator * rhs.numerator));
        znam = lhs.denominator * rhs.denominator;
        return new Fraction(dif, znam);
    }
}
