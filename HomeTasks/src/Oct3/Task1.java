package Oct3;

/*
Создать класс “Point” с двумя вещественными переменными для определения координат точки по x и
y. Добавить в класс конструктор с параметрами, две функции для возвращения значений координат x и y
соответственно, а также функцию, в которую передаются координаты другой точки, после чего в ней
рассчитывается расстояние до нее и выводится на экран.
*/

public class Task1 {
    public static void main(String[] args) {
        Point point = new Point(5, 7);
        System.out.println("Координаты точки x: " + point.getX() + ", y: " + point.getY());
        Point point2 = new Point(8.2, 12.3);
        System.out.println("Координаты второй точки x: " + point2.x + ", y: " + point2.y);
        System.out.printf("Расстояние между точками: %.2f", point.distance(point2.x, point2.y));

    }
}

class Point {
    double x;
    double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    double distance(double x, double y) {
        return Math.sqrt(Math.pow((x - this.x), 2) + (Math.pow((y - this.y), 2)));
    }
}
