package Oct3;

/*
Создать класс “Date” с тремя целочисленными показателями для определения дня, месяца и года.
Добавить в класс функции для определения по дате времени года, номера дня в году, а также функцию
для расчета количества дней между двумя датами, принимающую на вход объект данного класса.
*/

public class Task9 {
    public static void main(String[] args) {
        Date date = new Date(13, 9, 1987);
        System.out.println("Пора года: " + date.season());
        System.out.println("Номер дня в году первой даты: " + date.number());
        Date date2 = new Date(15,11,1988);
        System.out.println("Номер дня в году второй даты: " + date2.number());
        System.out.println("Количество дней между двумя датами: " + date.difference(date, date2));

    }
}
class Date{
    private int day;
    private int month;
    private int year;

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    String season() {
        String result = null;
        if (month > 2 && month < 6)
            result = "весна";
        if (month > 5 && month < 9)
            result = "лето";
        if (month > 8 && month < 12)
            result = "осень";
        if (month > 11 || month < 3)
            result = "зима";

        return result;
    }

    int number() {
        int day = 0;
        int x = 0;
        if (year % 4 == 0 & year % 100 != 0 | year % 400 == 0)
            x++;
        else
            x = 0;

        if (month == 1)
            day = this.day;
        if (month == 2)
            day = 31 + this.day;
        if (month >= 3 && month <= 7)
            day = (month -1) * 31 - (month -1) / 2 - 2 + x + this.day;
        if (month > 7 && month <= 12)
            if (month % 2 == 0)
                day = (month -1) * 31 - (month -1) / 2 - 2 + x + this.day;
            else
                day = (month -1) * 31 - (month -1) / 2 - 1 + x + this.day;


        return day;
    }

    int yearToDays() {
        int everyDays = 0;
        for (int i = 0; i < year; i++) {
            if (i % 4 == 0 & i % 100 != 0 | i % 400 == 0)
                everyDays += 366;
            else
                everyDays += 365;
        }
        return everyDays;
    }

    int difference(Date date, Date date2) {
        int dif = 0;
        if (date.year > date2.year)
            dif = (date.number() + date.yearToDays()) - (date2.number() + date2.yearToDays());
        if (date2.year > date.year)
            dif = (date2.number() + date2.yearToDays()) - (date.number() + date.yearToDays());
        if (date2.year == date.year) {
            if (date.number() > date2.number())
                dif = date.number() - date2.number();
            if (date2.number() > date.number())
                dif = date2.number() - date.number();
        }
        return dif;
    }
}
