package Oct3;

/*
Создать класс “Figure” с тремя вещественными показателями для длины, ширины и высоты фигуры.
Создать три конструктора, которые принимают от одного до трех указанных выше параметров для
создания объектов класса, представляющих соответственно прямую, прямоугольник и параллелепипед.
Добавить в класс функцию для вывода типа фигуры в соответствии с определенными в конструкторе
параметрами.
*/

public class Task7 {
    public static void main(String[] args) {
        Figure figure = new Figure(2.5);
        System.out.println("Тип фигуры: " + figure.type(2.5));
        System.out.println("Тип фигуры: " + figure.type(2.5, 7.3));
        System.out.println("Тип фигуры: " + figure.type(2.5, 7.3, 4.6));
    }
}

class Figure {
    double a;
    double b;
    double c;

    public Figure(double a) {
        this.a = a;
    }

    public Figure(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public Figure(double a, double b, double c) {
        this(a, b);
        this.c = c;
    }

    String type(double a) {
        return "квадрат";
    }

    String type(double a, double b) {
        return "параллелепипед";
    }

    String type(double a, double b, double c) {
        return "треугольник";
    }
}
