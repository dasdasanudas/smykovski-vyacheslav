package Oct3;

/*
Создать класс “Triangle” с тремя вещественными переменными для задания длин его сторон.
Добавить в класс функции для расчета периметра, площади по формуле Герона, а также для определения
типа треугольника.
*/

public class Task3 {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(5, 4, 3);
        System.out.println("Длина сторон треугольника a: " + triangle.a + ", b: " + triangle.b + ", c: " + triangle.c);
        System.out.println("Периметр треугольника: " + triangle.perimeter(triangle.a, triangle.b, triangle.c));
        System.out.println("Площадь по формуле Герона: " + triangle.square(triangle.a, triangle.b, triangle.c));
        System.out.print("Тип треугольника: ");
        triangle.type(triangle.a, triangle.b, triangle.c);
    }
}

class Triangle {
    double a;
    double b;
    double c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
        if (a + b < c | b + c < a)
            System.out.println("Такого треугольника не существует");
    }

    public double geta() {
        return a;
    }

    public double getb() {
        return b;
    }

    public double getc() {
        return c;
    }

    double perimeter(double a, double b, double c) {
        return a + b + c;
    }

    double square(double a, double b, double c) {
        return Math.sqrt(perimeter(a, b, c) / 2 * (perimeter(a, b, c) / 2 - a) * (perimeter(a, b, c) / 2 - b) * (perimeter(a, b, c) / 2 - c));
    }

    void type(double a, double b, double c) {
        double cornerA = Math.asin(2 * square(a, b, c) / (a * b)) * 180 / Math.PI;
        double cornerB = Math.asin(2 * square(a, b, c) / (a * c)) * 180 / Math.PI;
        double cornerC = Math.asin(2 * square(a, b, c) / (b * c)) * 180 / Math.PI;
        if (a == b && b == c)
            System.out.println("равносторонний");
        if (a == b || a == c || b == c)
            System.out.println("равнобедренный");
        if (cornerA == 90 || cornerB == 90 || cornerC == 90)
            System.out.println("прямоугольный");
        else if (cornerA > 90 || cornerB > 90 || cornerC > 90)
            System.out.println("тупой");
        else if (cornerA < 90 || cornerB < 90 || cornerC < 90)
            System.out.println("острый");
    }
}
