package Nov13;

/*
Числа, запись которых состоит из двух одинаковых последовательностей цифр, называются симметричными.
Например, 357357 или 17421742. Определите, является ли данное натуральное число симметричным.
*/

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Write the number: ");
        System.out.print(new Symmetric().isSymmetric(String.valueOf(scanner.nextInt())));

    }
}

class Symmetric {
    String symmetric = "NO";

    public String isSymmetric(String s) {
        if (s.length() > 0 && s.length() % 2 == 0) {
            if (s.substring(0, s.length() / 2).equals(s.substring(s.length() / 2))) {
                symmetric = "Your number is symmetric!";
            } else {
                symmetric = "Your number isn't symmetric!";
            }
        }
        return symmetric;
    }
}
