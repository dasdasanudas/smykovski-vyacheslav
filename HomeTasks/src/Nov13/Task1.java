package Nov13;

/*
Два нечетных простых числа, отличающиеся на 2, называются близнецами. Например, числа 5 и 7.
Напишите программу, которая будет находить все числа-близнецы на отрезке [2; 1000].
*/

import java.util.ArrayList;

public class Task1 {
    public static void main(String[] args) {
    Twins twin = new Twins();
    twin.create(2, 1000);
    twin.print();
    }
}
class Twins {
    private int a;
    private int b;
    private ArrayList<Twins> list = new ArrayList<>();

    public Twins(int a) {
        this.a = a;
        b = a + 2;
    }

    public Twins() {
    }

    void create(int x, int y) {
        for (int i = x; i <= y; i++) {
            if (i % 2 != 0 && i < y - 2)
                list.add(new Twins(i));
        }
    }

    void print() {
        for (Twins twins : list) {
            System.out.println(twins);
        }
    }

    @Override
    public String toString() {
        return a + " \t" + b;
    }
}
