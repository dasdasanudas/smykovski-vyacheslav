package Nov13;

/*
Числа, одинаково читающиеся слева направо и справа налево, называются палиндромами.
Например, 1223221. Напишите программу нахождения всех палиндромов на данном отрезке.
*/

import java.util.ArrayList;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Write start number: ");
        int x = scanner.nextInt();
        System.out.print("Write finish number: ");
        int y = scanner.nextInt();
        Palindrome palindrome = new Palindrome();
        palindrome.find(x, y);
        palindrome.print();
    }
}

class Palindrome {
    int x;
    boolean palindrome = true;
    ArrayList<Integer> list = new ArrayList<>();

    public void find(int x, int y) {
        this.x = x;
        for (int i = x; i <= y; i++) {
            if (isPalindrome(String.valueOf(i))){
                list.add(i);
            }
        }
    }

    private boolean isPalindrome(String s) {
        if (s.length() > 1) {
            if (s.charAt(0) == s.charAt(s.length() - 1)) {
                isPalindrome(s.substring(1, s.length() - 1));
            } else {
                palindrome = false;
            }
        } else {
            palindrome = true;
        }
        return palindrome;
    }

    public void print() {
        for (Integer element : list) {
            System.out.println(element);
        }
    }
}
