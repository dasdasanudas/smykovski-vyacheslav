package Nov13;

/*
Найдите количество и сумму цифр в данном натуральном числе.
*/

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Sign the number: ");
        String s = scanner.nextLine();

        System.out.println("Number length: " + s.length());
        Number number = new Number();
        number.summary(Integer.parseInt(s));
        number.print();

    }
    static class Number {
        int sum = 0;
        public int summary(int x) {
            if (x > 0) {
                if (x % 10 != 0) {
                    sum += x % 10;
                }
                return summary(x / 10);
            }
            return sum;
        }

        public void print() {
            System.out.println("Sum of number: " + sum);
        }
    }
}
