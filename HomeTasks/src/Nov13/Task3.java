package Nov13;

/*
Дано натуральное число. Поменяйте в нем порядок цифр на обратный
*/

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Write the number: ");
        System.out.print("Reverse number: ");
        reverse(scanner.nextInt());
    }

    public static void reverse(int x){
        int sum;
        if (x > 0) {
            sum = x % 10;
            System.out.print(sum);
            reverse(x / 10);
        }
    }
}
