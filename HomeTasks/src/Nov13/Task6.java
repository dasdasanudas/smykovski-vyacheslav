package Nov13;

/*
Если сложить все цифры какого-либо натурального числа, затем — все цифры найденной суммы
и так далее, то в результате получим однозначное число (цифру), которое называется цифровым корнем
данного числа. Например, цифровой корень числа 561 равен 3 (5 + 6+1 — 12, 1+2 = 3).
Написать программу для нахождения числового корня данного натурального числа.
*/

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Write the number: ");
        System.out.println(new DigitalRoot().find(scanner.nextInt()));
    }
}
class DigitalRoot {
    public int find(int x) {
        int root = 0;
        if (String.valueOf(x).length() < 2)
            root = x;
        else {
            for (int i = 0; i <String.valueOf(x).length(); i++) {
                root = root + Character.getNumericValue(String.valueOf(x).charAt(i));
            }
            return find(root);
        }
        return root;
    }
}
