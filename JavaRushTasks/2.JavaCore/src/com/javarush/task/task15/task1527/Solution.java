package com.javarush.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/* 
Парсер реквестов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //add your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] list = new URL(reader.readLine()).getQuery().split("&");
        for (String s : list) {
            if (!s.contains("="))
                System.out.print(s + " ");
            else
                System.out.print(s.substring(0, s.indexOf('=') + 1).replace('=', ' '));
        }
        for (String s : list) {
            if (s.contains("obj")) {
                try {
                    double i = Double.parseDouble(s.substring(s.indexOf('=') + 1));
                    System.out.println();
                    alert(i);
                } catch (Exception ex) {
                    System.out.println();
                    alert(s.substring(s.indexOf('=') + 1));
                }
            }
        }
    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }
}
