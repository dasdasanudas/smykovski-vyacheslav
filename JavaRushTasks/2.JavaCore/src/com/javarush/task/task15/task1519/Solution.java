package com.javarush.task.task15.task1519;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;

/* 
Разные методы для разных типов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //напиште тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<>();

        String s = reader.readLine();
        while (!s.equals("exit")) {
            list.add(s);
            s = reader.readLine();
        }

        for (String o : list) {
            try {
            if (o.contains(".")) {
                print(Double.parseDouble(o));
            }
             else if (Integer.parseInt(o) <= 0 || Integer.parseInt(o) >= 128) {
                print((Integer) Integer.parseInt(o));
            }
            else if (Integer.parseInt(o) > 0 && Integer.parseInt(o) < 128) {
                print((short) Integer.parseInt(o));
            }
        }catch(Exception e){
                print(o);
            }
        }
    }

    public static void print(Double value) {
        System.out.println("Это тип Double, значение " + value);
    }

    public static void print(String value) {
        System.out.println("Это тип String, значение " + value);
    }

    public static void print(short value) {
        System.out.println("Это тип short, значение " + value);
    }

    public static void print(Integer value) {
        System.out.println("Это тип Integer, значение " + value);
    }
}
