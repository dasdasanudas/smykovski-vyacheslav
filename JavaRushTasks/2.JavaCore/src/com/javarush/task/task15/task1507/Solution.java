package com.javarush.task.task15.task1507;

/* 
ООП - Перегрузка
*/

public class Solution {
    public static void main(String[] args) {
        printMatrix((byte) 2, (byte) 3, "8");
    }

    public static void printMatrix(byte m, byte n, String value){
        printMatrix((short) m, (short) n, value);
    }
    public static void printMatrix(short m, short n, String value){
        printMatrix((int)n, (int) m, value);
    }
    public static void printMatrix(int m, int n, String value) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(value);
            }
            System.out.println();
        }
        printMatrix((long) m, n, value);
    }
    public static void printMatrix(long m, long n, String value){
        printMatrix((float)m, n, value);
    }
    public static void printMatrix(float m, float n, String value){
        printMatrix((double)m, n, value);
    }
    public static void printMatrix(double m, double n, String value){
        printMatrix(m, n, (Object) value);
    }
    public static void printMatrix(double m, double n, Object value) {
        System.out.println("Заполняем объектами String");
        printMatrix(m, (Double) n, value);
    }
    public static void printMatrix(double m, Double n, Object value){
        printMatrix((Double) m, n, value);
    }
    public static void printMatrix(Double m, Double n, Object value){
        printMatrix((Object) m, n, value);
    }
    public static void printMatrix(Object m, Object n, Object value){
        System.out.println(m + "" + n + value);
    }
}
