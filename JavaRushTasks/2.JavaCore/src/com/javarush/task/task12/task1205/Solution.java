package com.javarush.task.task12.task1205;

/* 
Определимся с животным
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(getObjectType(new Cow()));
        System.out.println(getObjectType(new Dog()));
        System.out.println(getObjectType(new Whale()));
        System.out.println(getObjectType(new Pig()));
    }

    public static String getObjectType(Object o) {
        //Напишите тут ваше решение
        if (o.getClass().getSimpleName().equals(Cow.class.getSimpleName()))
            return "Корова";
        if (o.getClass().getSimpleName().equals(Dog.class.getSimpleName()))
            return "Собака";
        if (o.getClass().getSimpleName().equals(Whale.class.getSimpleName()))
            return "Кит";
        if (o.getClass().getSimpleName().equals(Pig.class.getSimpleName()))
            return "Неизвестное животное";
        return "";
    }

    public static class Cow {
    }

    public static class Dog {
    }

    public static class Whale {
    }

    public static class Pig {
    }
}
