package com.javarush.task.task14.task1408;

/* 
Куриная фабрика
*/

public class Solution {
    public static void main(String[] args) {
        Hen hen = HenFactory.getHen(Country.UKRAINE);
        hen.getCountOfEggsPerMonth();
//        System.out.println(hen.getDescription());
    }

    static class HenFactory {

        static Hen getHen(String country) {
            Hen hen = null;
            //напишите тут ваш код
            if (country.equals(Country.BELARUS)) {
                return hen = new BelarusianHen();
            }
            if (country.equals(Country.MOLDOVA)) {
                return hen = new MoldovanHen();
            }
            if (country.equals(Country.RUSSIA)) {
                return hen = new RussianHen();
            } else
                return hen = new UkrainianHen();
        }
    }
}
