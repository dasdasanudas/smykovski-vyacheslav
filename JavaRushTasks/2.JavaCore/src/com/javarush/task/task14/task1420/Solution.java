package com.javarush.task.task14.task1420;

/* 
НОД
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int x = Integer.parseInt(reader.readLine());
        if (x <= 0) throw new Exception();
        int y = Integer.parseInt(reader.readLine());
        if (y <= 0) throw new Exception();
        nod(x, y);

    }
    static void nod(int x, int y){
        int noda = 1;
        for (int i = noda; i <= Math.min(x, y); i++) {
            if (x % i == 0 && y % i == 0) {
                noda = i;
            }
        }
        System.out.println(noda);
    }
}
