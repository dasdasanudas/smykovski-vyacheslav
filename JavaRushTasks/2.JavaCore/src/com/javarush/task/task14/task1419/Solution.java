package com.javarush.task.task14.task1419;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/* 
Нашествие исключений
*/

public class Solution {
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args) {
        initExceptions();

        for (Exception exception : exceptions) {
            System.out.println(exception);
        }
    }

    private static void initExceptions() {   //the first exception
        try {
            float i = 1 / 0;

        } catch (Exception e) {
            exceptions.add(e);
        }

        //напишите тут ваш код
        try {
            Object object = new Object();
            object = null;
            System.out.println(object.toString());
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            char[] arr = new char[0];
            System.out.println(arr[1]);
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            FileInputStream fileInputStream = new FileInputStream("D:zzz.txt");
            System.out.println(fileInputStream.read());
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            String s = "qwertyu";
            System.out.println(Integer.parseInt(s));
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            ArrayList<String> list = new ArrayList<>(0);
            System.out.println(list.get(1));
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            int[] arr = new int[-1];
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            String s = "qwerty";
            System.out.println(s.charAt(7));
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            Object object = (Solution) new Object();
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            FileWriter fileWriter = new FileWriter("out.txt");
            fileWriter.close();
            fileWriter.write("Hello World!");
        } catch (Exception e) {
            exceptions.add(e);
        }
    }
}
