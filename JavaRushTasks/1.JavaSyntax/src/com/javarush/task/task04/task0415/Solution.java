package com.javarush.task.task04.task0415;

/* 
Правило треугольника
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader  reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("");
        String s = reader.readLine();
        int a = Integer.parseInt(s);
        System.out.print("");
        s = reader.readLine();
        int b = Integer.parseInt(s);
        System.out.print("");
        s = reader.readLine();
        int c = Integer.parseInt(s);

        if (a + b > c & b + c > a & a + c > b)
            System.out.println("Треугольник существует.");
        else
            System.out.println("Треугольник не существует.");
    }
}