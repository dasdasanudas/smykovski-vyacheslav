package com.javarush.task.task04.task0442;


/* 
Суммирование
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("");
        String s = reader.readLine();
        int a = Integer.parseInt(s);
        int sum = 0;

        while (a != -1) {
            sum += a;
            System.out.print("");
            s = reader.readLine();
            a = Integer.parseInt(s);
        }
        System.out.println(sum - 1);
    }
}