package com.javarush.task.task04.task0421;

/* 
Настя или Настя?
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("");
        String a = reader.readLine();
        System.out.print("");
        String b = reader.readLine();

        if (a.equals(b))
            System.out.println("Имена идентичны");
        else if (a.length() == b.length())
            System.out.println("Длины имен равны");
    }
}
