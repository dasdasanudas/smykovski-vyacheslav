package com.javarush.task.task04.task0418;

/* 
Минимум двух чисел
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("");
        String s = reader.readLine();
        int a = Integer.parseInt(s);
        System.out.print("");
        s = reader.readLine();
        int b = Integer.parseInt(s);

        if (a < b)
            System.out.println(a);
        else
            System.out.println(b);

    }
}