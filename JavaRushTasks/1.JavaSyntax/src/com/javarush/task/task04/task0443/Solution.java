package com.javarush.task.task04.task0443;


/* 
Как назвали, так назвали
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("");
        String name = reader.readLine();
        System.out.print("");
        String s = reader.readLine();
        int y = Integer.parseInt(s);
        System.out.print("");
        s = reader.readLine();
        int m = Integer.parseInt(s);
        System.out.print("");
        s = reader.readLine();
        int d = Integer.parseInt(s);

        System.out.println("Меня зовут " + name + ".");
        System.out.println("Я родился " + d + "." + m + "." + y);

    }
}
