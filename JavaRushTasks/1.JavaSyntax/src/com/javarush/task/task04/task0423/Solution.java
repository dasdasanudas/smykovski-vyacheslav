package com.javarush.task.task04.task0423;

/* 
Фейс-контроль
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("");
        String s = reader.readLine();
        System.out.print("");
        String b = reader.readLine();
        int a = Integer.parseInt(b);

        if (a > 20)
            System.out.println("И 18-ти достаточно");
    }
}
