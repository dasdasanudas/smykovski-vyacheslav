package com.javarush.task.task04.task0416;

/* 
Переходим дорогу вслепую
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("");
        String s = reader.readLine();
        double t = Double.parseDouble(s);

        while (t > 0 & t > 5) {
            t = t - 5;
        }
        if (t > 0 & t < 3 | t == 5)
            System.out.println("зелёный");
        else if (t >= 3 & t < 4)
            System.out.println("жёлтый");
        else if (t >= 4 & t < 5)
            System.out.println("красный");
        else
            System.out.println("не может быть отрицательным");
    }
}