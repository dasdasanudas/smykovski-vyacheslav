package com.javarush.task.task04.task0429;

/* 
Положительные и отрицательные числа
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("");
        String s = reader.readLine();
        int a = Integer.parseInt(s);
        System.out.print("");
        s = reader.readLine();
        int b = Integer.parseInt(s);
        System.out.print("");
        s = reader.readLine();
        int c = Integer.parseInt(s);
        int num = 0;
        int mun = 0;

        if (a > 0)
            num++;
        if (b > 0)
            num++;
        if (c > 0)
            num++;
        if (a < 0)
            mun++;
        if (b < 0)
            mun++;
        if (c < 0)
            mun++;
        System.out.println("количество отрицательных чисел: " + mun);
        System.out.println("количество положительных чисел: " + num);
    }
}
