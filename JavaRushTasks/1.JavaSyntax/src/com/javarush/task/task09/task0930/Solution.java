package com.javarush.task.task09.task0930;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/* 
Задача по алгоритмам
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<>();
        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) break;
            list.add(s);
        }

        String[] array = list.toArray(new String[0]);
        sort(array);

        for (String x : array) {
            System.out.println(x);
        }
    }

    public static void sort(String[] array) {
        // напишите тут ваш код
        ArrayList<String> arrayStr = new ArrayList<>();
        ArrayList<Integer> arrayNum = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            if (!isNumber(array[i])) {
                arrayStr.add(array[i]);
            } else
                arrayNum.add(Integer.parseInt(array[i]));
        }
        String tmp;
        for (int i = 0; i < arrayStr.size() - 1; i++) {
            for (int j = 0; j < arrayStr.size() - 1 - i; j++) {
                if (isGreaterThan(arrayStr.get(j), arrayStr.get(j + 1))) {
                    tmp = arrayStr.get(j);
                    arrayStr.set(j, arrayStr.get(j + 1));
                    arrayStr.set(j + 1, tmp);
                }
            }
        }
        Collections.sort(arrayNum);

        int countNum = arrayNum.size() - 1;
        int countStr = 0;
        for (int i = 0; i < array.length; i++) {
            if (!isNumber(array[i])) {
                array[i] = arrayStr.get(countStr);
                countStr++;
            } else {
                array[i] = String.valueOf(arrayNum.get(countNum));
                countNum--;
            }
        }
    }

    // Метод для сравнения строк: 'а' больше чем 'b'
    public static boolean isGreaterThan(String a, String b) {
        return a.compareTo(b) > 0;
    }


    // Переданная строка - это число?
    public static boolean isNumber(String s) {
        if (s.length() == 0) return false;

        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if ((i != 0 && c == '-') // Строка содержит '-'
                    || (!Character.isDigit(c) && c != '-') // или не цифра и не начинается с '-'
                    || (chars.length == 1 && c == '-')) // или одиночный '-'
            {
                return false;
            }
        }
        return true;
    }
}
