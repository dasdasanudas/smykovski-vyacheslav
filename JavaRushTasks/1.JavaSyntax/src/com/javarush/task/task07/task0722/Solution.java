package com.javarush.task.task07.task0722;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Это конец
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        //напишите тут ваш код
        ArrayList<String> strings = new ArrayList<>();
        String s;
        do {
            s = reader.readLine();
            if (s.equals("end")) {
                break;
            }
            strings.add(s);
        }
        while (!s.equals("end"));
        for (String x : strings
        ) {
            System.out.println(x);
        }
    }
}