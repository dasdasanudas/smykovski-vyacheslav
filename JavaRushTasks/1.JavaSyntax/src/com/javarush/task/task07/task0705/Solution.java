package com.javarush.task.task07.task0705;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Один большой массив и два маленьких
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] arr = new int[20];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(reader.readLine());
        }
        int[] small1 = new int[10];
        int[] small2 = new int[10];
        System.arraycopy(arr, 0, small1, 0, 10);
        System.arraycopy(arr, 10, small2, 0, 10);
        for (int x: small2){
            System.out.println(x);
        }
    }
}
