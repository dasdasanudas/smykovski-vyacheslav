package com.javarush.task.task07.task0709;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Выражаемся покороче
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            strings.add(reader.readLine());
        }
        String min = strings.get(0);
        for (int i = 0; i < strings.size() -1; i++) {
            if (strings.get(i).length() > strings.get(i+1).length()) {
                min = strings.get(i+1);
            }
        }
        for (int i = 0; i < strings.size(); i++) {
            if (strings.get(i).length() == min.length()){
                System.out.println(strings.get(i));
            }
        }
    }
}
