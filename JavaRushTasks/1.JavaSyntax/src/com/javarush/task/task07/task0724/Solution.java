package com.javarush.task.task07.task0724;

/* 
Семейная перепись
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        Human grandpa1 = new Human("Миша", true, 80);
        Human grandpa2 = new Human("Йозеф", true, 80);
        Human granny1 = new Human("Надя", false, 80);
        Human granny2 = new Human("Аня", false, 80);
        Human father = new Human("Толя", true, 80, grandpa2, granny2);
        Human mother = new Human("Таня", false, 80, grandpa1, granny1);
        Human son = new Human("Слава", true, 80, father, mother);
        Human dauther1 = new Human("Ира", false, 80, father, mother);
        Human dauther2 = new Human("Ира", false, 80, father, mother);
        System.out.println(grandpa1.toString());
        System.out.println(grandpa2.toString());
        System.out.println(granny1.toString());
        System.out.println(granny2.toString());
        System.out.println(father.toString());
        System.out.println(mother.toString());
        System.out.println(son.toString());
        System.out.println(dauther1.toString());
        System.out.println(dauther2.toString());

    }

    public static class Human {
        //напишите тут ваш код
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public Human(String name, boolean sex, int age, Human father, Human mother) {
            this(name, sex, age);
            this.father = father;
            this.mother = mother;
        }

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }
}






















