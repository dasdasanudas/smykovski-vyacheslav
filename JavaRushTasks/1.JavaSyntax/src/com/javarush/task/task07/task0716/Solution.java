package com.javarush.task.task07.task0716;

import java.util.ArrayList;

/* 
Р или Л
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<String>();
        list.add("роза"); //0
        list.add("лоза"); //1
        list.add("лира"); //2
        list = fix(list);

        for (String s : list) {
            System.out.println(s);
        }
    }

    public static ArrayList<String> fix(ArrayList<String> list) {
        //напишите тут ваш код
        ArrayList<String> list2 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            boolean flagR = false, flagL = false;
            for (int j = 0; j < list.get(i).length(); j++) {

                if (list.get(i).charAt(j) == 'л') {
                    flagL = true;
                }
                if (list.get(i).charAt(j) == 'р') {
                    flagR = true;
                }

            }
            if (flagL && flagR) {
                list2.add(list.get(i));
            }
            if (flagL && !flagR) {
                list2.add(list.get(i));
                list2.add(list.get(i));
            }
            if (!flagL && !flagR) {
                list2.add(list.get(i));
            }
        }
/*        for (int i = 0; i < list.size(); ) {
            for (int j = 0; j < list.get(i).length(); j++) {

               if (list.get(i).charAt(j) == 'р' && list.get(i).charAt(j) == 'л') {
                    i++;
                }
                if (list.get(i).charAt(j) == 'р') {
                    list.remove(i);
                }
                if (list.get(i).charAt(j) == 'л') {
                    list.add(i + 1, list.get(i));
                    i++;
                }
            }
        }*/
        return list2;
    }
}