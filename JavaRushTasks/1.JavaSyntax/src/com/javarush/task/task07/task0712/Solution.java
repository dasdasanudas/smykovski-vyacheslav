package com.javarush.task.task07.task0712;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Самые-самые
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> strings = new ArrayList<>();
        int min = 0;
        int max = 0;
        for (int i = 0; i < 10; i++) {
            strings.add(reader.readLine());
        }
        for (int i = 0; i < strings.size(); i++) {
            if (strings.get(min).length() > strings.get(i).length()) {
                min = i;
            }
            if (strings.get(i).length() > strings.get(max).length())
                max = i;
            }
        if (min < max){
            System.out.println(strings.get(min));
        }
        else {
            System.out.println(strings.get(max));
        }
    }
}
