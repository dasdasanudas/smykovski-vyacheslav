package com.javarush.task.task07.task0708;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Самая длинная строка
*/

public class Solution {
    private static ArrayList<String> strings = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < 5; i++) {
            strings.add(reader.readLine());
        }
        String max = strings.get(0);
        for (int i = 0; i < strings.size() -1; i++) {
            if (strings.get(i).length() < strings.get(i+1).length()) {
                max = strings.get(i+1);
            }
        }
        for (int i = 0; i < strings.size(); i++) {
            if (strings.get(i).length() == max.length()){
                System.out.println(strings.get(i));
            }
        }
    }
}
