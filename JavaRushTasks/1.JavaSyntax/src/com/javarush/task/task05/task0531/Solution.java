package com.javarush.task.task05.task0531;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Совершенствуем функциональность
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        int d = Integer.parseInt(reader.readLine());
        int e = Integer.parseInt(reader.readLine());

        System.out.println("Minimum = " + min(a, b, c, d, e));
    }


    public static int min(int a, int b, int c, int d, int e) {
        int[] minimum = new int[]{a, b, c, d, e};
        for (int j = 0; j < minimum.length; j++) {
            for (int i = 0; i < minimum.length - 1 - j; i++) {
                if (minimum[i + 1] <= minimum[i]) {
                    int min = minimum[i];
                    minimum[i] = minimum[i + 1];
                    minimum[i + 1] = min;
                }
            }
        }
        return minimum[0];
    }
}