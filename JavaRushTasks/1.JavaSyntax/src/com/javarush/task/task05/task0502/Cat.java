package com.javarush.task.task05.task0502;

/* 
Реализовать метод fight
*/

public class Cat {
    public int age;
    public int weight;
    public int strength;

    public Cat() {

    }

    public boolean fight(Cat anotherCat) {
        //напишите тут ваш код
        if (this.age > anotherCat.age | this.strength > anotherCat.strength | this.weight > anotherCat.weight)
            return true;
        else
            return false;

    }

    public static void main(String[] args) {
        Cat cat1 = new Cat();
        Cat cat2 = new Cat();
        cat1.age = 10;
        cat1.weight = 7;
        cat1.strength = 100;
        cat2.age = 5;
        cat2.weight = 5;
        cat2.strength = 70;
        cat1.fight(cat2);
//        cat2.fight(cat1);
    }
}
