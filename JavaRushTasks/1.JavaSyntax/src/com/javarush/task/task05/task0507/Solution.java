package com.javarush.task.task05.task0507;
import java.io.*;

/* 
Среднее арифметическое
*/

public class Solution {
    public static void main(String[] args) throws Exception {
    //напишите тут ваш код
        double sum = 0.0;
        double x;
        double count = 0.0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        do {
            System.out.print("");
            String s = reader.readLine();
            x = Integer.parseInt(s);
            sum += x;
            count++;
        }
        while (x != -1);
        System.out.println((sum + 1) / (count - 1));
    }
}

