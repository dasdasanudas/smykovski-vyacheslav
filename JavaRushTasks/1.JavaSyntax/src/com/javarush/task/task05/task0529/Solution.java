package com.javarush.task.task05.task0529;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Консоль-копилка
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        String s;
        int x = 0;
        boolean flag = true;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (flag) {
            System.out.print("");
            s = reader.readLine();
            if (s.equals("сумма")) {
                System.out.println(x);
                flag = false;
                break;
            }
            x += Integer.parseInt(s);
        }
    }
}