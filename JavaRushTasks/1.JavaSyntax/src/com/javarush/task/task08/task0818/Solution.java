package com.javarush.task.task08.task0818;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* 
Только для богачей
*/

public class Solution {
    public static HashMap<String, Integer> createMap() {
        //напишите тут ваш код
    HashMap<String, Integer> map = new HashMap<>();
        for (int i = 1; i <= 10; i++) {
            map.put("Family" + i, 100 * i);
        }
        return map;
}

    public static void removeItemFromMap(HashMap<String, Integer> map) {
        //напишите тут ваш код
        //map.entrySet().removeIf(entry -> entry.getValue() < 500);
        Iterator<HashMap.Entry<String, Integer>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getValue() < 500) {
                iterator.remove();
            }
        }

        //map.forEach((key, value) -> System.out.println(key + " - " + value));
    }

    public static void main(String[] args) {
    }
}