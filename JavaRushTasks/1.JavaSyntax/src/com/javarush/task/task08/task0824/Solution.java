package com.javarush.task.task08.task0824;

/* 
Собираем семейство
*/

import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        Human son = new Human("Slava", true, 31);
        Human dauther1 = new Human("fff", false, 27);
        Human dauther2 = new Human("mmm", false, 25);
        ArrayList<Human> childs = new ArrayList<>();
        childs.add(son);
        childs.add(dauther1);
        childs.add(dauther2);
        Human father = new Human("Tolya", true, 55, childs);
        Human mother = new Human("Tanya", false, 53, childs);
        Human grandFather1 = new Human("Misha", true, 85, father);
        Human grandFather2 = new Human("Josef", true, 83, mother);
        Human granny1 = new Human("Nastya", false, 82, father);
        Human granny2 = new Human("Anna", false, 80, mother);

        System.out.println(grandFather1.toString());
        System.out.println(grandFather2.toString());
        System.out.println(granny1.toString());
        System.out.println(granny2.toString());
        System.out.println(father.toString());
        System.out.println(mother.toString());
        System.out.println(son.toString());
        System.out.println(dauther1.toString());
        System.out.println(dauther2.toString());

    }

    public static class Human {
        //напишите тут ваш код
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children = new ArrayList<>();

        public Human(String name, boolean sex, int age, ArrayList<Human> children) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }

        public Human(String name, boolean sex, int age, Human child) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            children.add(child);
        }

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0) {
                text += ", дети: " + this.children.get(0).name;

                for (int i = 1; i < childCount; i++) {
                    Human child = this.children.get(i);
                    text += ", " + child.name;
                }
            }
            return text;
        }
    }

}
