package com.javarush.task.task08.task0812;

import java.io.*;
import java.util.ArrayList;

/* 
Cамая длинная последовательность
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(Integer.parseInt(reader.readLine()));
        }
        int count = 1;
        int count2 = 1;
        for (int i = 0; i < list.size() - 1; i++) {
            if (list.get(i).intValue() == list.get(i + 1).intValue()) {
                count++;
            }
            else {
                    count = 1;
                }
            if (count2 < count) {
                count2 = count;
            }
        }
        System.out.println(count2);
    }
}