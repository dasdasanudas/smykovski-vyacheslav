package com.javarush.task.task08.task0816;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

//Добрая Зинаида и летние каникулы

public class Solution {
    public static HashMap<String, Date> createMap() throws ParseException {
        DateFormat df = new SimpleDateFormat("MMMMM d yyyy", Locale.ENGLISH);
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Stallone", df.parse("JUNE 1 1980"));
        map.put("AAAA", df.parse("APRIL 1 1980"));
        map.put("DDDDe", df.parse("JULY 1 1980"));
        map.put("BBBB", df.parse("AUGUST 1 1980"));
        map.put("CCCCC", df.parse("SEPTEMBER 1 1980"));
        map.put("EEEEE", df.parse("OCTOBER 1 1980"));
        map.put("MMMMM", df.parse("NOVEMBER 1 1980"));
        map.put("SJGR", df.parse("DECEMBER 1 1980"));
        map.put("BCKLS", df.parse("FEBRUARY 1 1980"));
        map.put("DJMVY", df.parse("MAY 1 1980"));

        //напишите тут ваш код
        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map) {
        //напишите тут ваш код
        Iterator<HashMap.Entry<String, Date>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getValue().getMonth() == 5 || iterator.next().getValue().getMonth() == 6 || iterator.next().getValue().getMonth() == 7) {
                iterator.remove();
            }
        }
    }

    public static void main(String[] args) {

    }
}
