package com.javarush.task.task03.task0319;

/* 
Предсказание на будущее
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("");
        String name = bufferedReader.readLine();
        System.out.print("");
        String num1 = bufferedReader.readLine();
        System.out.print("");
        String num2 = bufferedReader.readLine();
        System.out.println(name + " получает " + num1 + " через " + num2 + " лет.");

    }
}
