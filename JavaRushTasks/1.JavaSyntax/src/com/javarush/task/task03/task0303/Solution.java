package com.javarush.task.task03.task0303;

/* 
Обмен валют
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(convertEurToUsd(100, 0.8537));
        System.out.println(convertEurToUsd(150, 0.8537));//напишите тут ваш код
    }

    public static double convertEurToUsd(int eur, double course) {
        double dollarUSA = eur * course;
        return dollarUSA;//напишите тут ваш код
    }
}
