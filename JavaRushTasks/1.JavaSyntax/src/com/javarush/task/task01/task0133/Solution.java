package com.javarush.task.task01.task0133;

/* 
Не думать о секундах…
*/

public class Solution {
    public static void main(String[] args) {
        int hourNow = 15;
        int hourBefore = 15;
        int minutesNow = 30;
        int minutesBefore = 0;
        int secondsAfter15 = ((hourNow - hourBefore) * 60 * 60) + ((minutesNow - minutesBefore) * 60);
        System.out.println(secondsAfter15);

    }
}