package com.javarush.task.task06.task0606;

import java.io.*;

/* 
Чётные и нечётные циферки
*/

public class Solution {

    public static int even;
    public static int odd;

    public static void main(String[] args) throws IOException {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("");
        String s = reader.readLine();
        char[] strToCh = s.toCharArray();
        for (int i = 0; i < strToCh.length; i++) {
            int x = ((int) strToCh[i] % 2 == 0) ? even++ : odd++;
        }
        System.out.println("Even: " + even +" Odd: " + odd);
    }
}
