package com.javarush.task.task06.task0621;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Родственные связи кошек
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//grandpa
        String grandpaName = reader.readLine();
        Cat catGrandpa = new Cat(grandpaName);
//granny
        String grannyName = reader.readLine();
        Cat catGranny = new Cat(grannyName);
//father
        String fatherName = reader.readLine();
        Cat catFather = new Cat(fatherName,null, catGrandpa);
//mother
        String motherName = reader.readLine();
        Cat catMother = new Cat(motherName, catGranny, null);
//son
        String sonName = reader.readLine();
        Cat catSon = new Cat(sonName, catMother, catFather);
//doter
        String daughterName = reader.readLine();
        Cat catDaughter = new Cat(daughterName, catMother, catFather);

        System.out.println(catGrandpa.toString());
        System.out.println(catGranny.toString());
        System.out.println(catFather.toString());
        System.out.println(catMother.toString());
        System.out.println(catSon.toString());
        System.out.println(catDaughter.toString());
    }

    public static class Cat {
        private String name;
        private Cat parent;
        private Cat parent2;

        Cat(String name) {
            this.name = name;
        }

        public Cat(String name, Cat parent, Cat parent2) {
            this.name = name;
            this.parent = parent;
            this.parent2 = parent2;
        }

        @Override
        public String toString() {
            if (parent == null && parent2 == null)
                return "The cat's name is " + name + ", no mother, no father ";
            else if (parent == null)
                return "The cat's name is " + name + ", no mother, father is " + parent2.name;
            else if (parent2 == null)
                return "The cat's name is " + name + ", mother is " + parent.name + ", no father";
            else
                return "The cat's name is " + name + ", mother is " + parent.name + ", father is " + parent2.name;
        }
    }

}
