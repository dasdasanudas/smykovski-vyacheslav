package com.javarush.task.task10.task1013;

/* 
Конструкторы класса Human
*/

public class Solution {
    public static void main(String[] args) {
    }

    public static class Human {
        // Напишите тут ваши переменные и конструкторы
        private String name;
        private int age;
        private char sex;
        private double weight;
        private float height;
        private boolean devotee;

        public Human(String name) {
            this.name = name;
        }

        public Human(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public Human(String name, int age, char sex) {
            this(name, age);
            this.sex = sex;
        }

        public Human(String name, int age, char sex, double weight) {
            this(name, age, sex);
            this.weight = weight;
        }

        public Human(String name, int age, char sex, double weight, float height) {
            this(name, age, sex, weight);
            this.height = height;
        }

        public Human(String name, int age, char sex, double weight, float height, boolean devotee) {
            this(name, age, sex, weight, height);
            this.devotee = devotee;
        }

        public Human(String name, char sex) {
            this.name = name;
            this.sex = sex;
        }

        public Human(String name, char sex, double weight) {
            this(name, sex);
            this.weight = weight;
        }

        public Human(String name, char sex, double weight, float height) {
            this(name, sex, weight);
            this.height = height;
        }

        public Human(String name, char sex, double weight, float height, boolean devotee) {
            this(name, sex, weight, height);
            this.devotee = devotee;
        }
    }
}
